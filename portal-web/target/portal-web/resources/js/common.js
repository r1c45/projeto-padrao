function jump(idAtual, tamanhoMaximo, idProximo) {
	var valorAtual = replaceAll(document.getElementById(idAtual).value, '_', '');
	valorAtual = replaceAll(valorAtual, '.', '');
	valorAtual = replaceAll(valorAtual, '-', '');

	if (valorAtual.length == tamanhoMaximo) {
		jumpTo(idProximo);
	}
}

function jumpTo(idProximo) {
	document.getElementById(idProximo).focus();
}

function replaceAll(string, token, newtoken) {
	while (string.indexOf(token) != -1) {
		string = string.replace(token, newtoken);
	}
	return string;
}

function printElement(element, title) {
	var mywindow = window.open('', title, 'height=600,width=800');
	mywindow.document.write($(element).html());

	mywindow.document.close(); // necessary for IE >= 10
	mywindow.focus(); // necessary for IE >= 10

	mywindow.print();
	mywindow.close();

	return true;
}

function removeAcento(elemento) {
	var valor = elemento.value;

	str_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
	str_sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
	var nova = "";
	for (var i = 0; i < valor.length; i++) {
		if (str_acento.indexOf(valor.charAt(i)) != -1) {
			nova += str_sem_acento.substr(
					str_acento.search(valor.substr(i, 1)), 1);
		} else {
			nova += valor.substr(i, 1);
		}
	}

	elemento.value = nova;
}

function removeEspaco(elemento) {
	var valor = elemento.value;
	
	valor = valor.replace(' ', '');
	
	elemento.value = valor;
}

function toLowerCase(elemento) {
	elemento.value = elemento.value.toLowerCase();
}

function toUpperCase(elemento) {
	elemento.value = elemento.value.toUpperCase();
}

function formataEmail(elemento){
	toLowerCase(elemento);
	elemento.value = elemento.value.trim();
}