INSERT INTO tb_banco VALUES ('001', 'Banco do Brasil S.A.');
INSERT INTO tb_banco VALUES ('003', 'Banco da Amazônia S.A.');
INSERT INTO tb_banco VALUES ('004', 'Banco do Nordeste do Brasil S.A.');
INSERT INTO tb_banco VALUES ('012', 'Banco INBURSA de Investimentos S.A.');
INSERT INTO tb_banco VALUES ('017', 'BNY Mellon Banco S.A.');
INSERT INTO tb_banco VALUES ('021', 'BANESTES S.A. Banco do Estado do Espírito Santo');
INSERT INTO tb_banco VALUES ('024', 'Banco BANDEPE S.A.');
INSERT INTO tb_banco VALUES ('025', 'Banco Alfa S.A.');
INSERT INTO tb_banco VALUES ('029', 'Banco Itaú BMG Consignado S.A.');
INSERT INTO tb_banco VALUES ('031', 'Banco Beg S.A.');
INSERT INTO tb_banco VALUES ('033', 'Banco Santander (Brasil) S.A.');
INSERT INTO tb_banco VALUES ('036', 'Banco Bradesco BBI S.A.');
INSERT INTO tb_banco VALUES ('037', 'Banco do Estado do Pará S.A.');
INSERT INTO tb_banco VALUES ('040', 'Banco Cargill S.A.');
INSERT INTO tb_banco VALUES ('041', 'Banco do Estado do Rio Grande do Sul S.A.');
INSERT INTO tb_banco VALUES ('045', 'Banco Opportunity de Investimento S.A.');
INSERT INTO tb_banco VALUES ('047', 'Banco do Estado de Sergipe S.A.');
INSERT INTO tb_banco VALUES ('062', 'Hipercard Banco Múltiplo S.A.');
INSERT INTO tb_banco VALUES ('063', 'Banco Bradescard S.A.');
INSERT INTO tb_banco VALUES ('064', 'Goldman Sachs do Brasil Banco Múltiplo S.A.');
INSERT INTO tb_banco VALUES ('065', 'Banco Andbank (Brasil) S.A.');
INSERT INTO tb_banco VALUES ('069', 'BPN Brasil Banco Múltiplo S.A.');
INSERT INTO tb_banco VALUES ('070', 'BRB - Banco de Brasília S.A.');
INSERT INTO tb_banco VALUES ('073', 'BB Banco Popular do Brasil S.A.');
INSERT INTO tb_banco VALUES ('074', 'Banco J. Safra S.A.');
INSERT INTO tb_banco VALUES ('075', 'Banco ABN AMRO S.A.');
INSERT INTO tb_banco VALUES ('078', 'Haitong Banco de Investimento do Brasil S.A.');
INSERT INTO tb_banco VALUES ('081', 'BBN Banco Brasileiro de Negócios S.A.');
INSERT INTO tb_banco VALUES ('082', 'Banco Topázio S.A.');
INSERT INTO tb_banco VALUES ('083', 'Banco da China Brasil S.A.');
INSERT INTO tb_banco VALUES ('094', 'Banco Petra S.A.');
INSERT INTO tb_banco VALUES ('095', 'Banco Confidence de Câmbio S.A.');
INSERT INTO tb_banco VALUES ('096', 'Banco BM&FBOVESPA de Serviços de Liquidação e Custódia S.A');
INSERT INTO tb_banco VALUES ('104', 'Caixa Econômica Federal');
INSERT INTO tb_banco VALUES ('107', 'Banco BBM S.A.');
INSERT INTO tb_banco VALUES ('118', 'Standard Chartered Bank (Brasil) S/A–Bco Invest.');
INSERT INTO tb_banco VALUES ('119', 'Banco Western Union do Brasil S.A.');
INSERT INTO tb_banco VALUES ('120', 'Banco Rodobens S.A.');
INSERT INTO tb_banco VALUES ('125', 'Brasil Plural S.A. - Banco Múltiplo');
INSERT INTO tb_banco VALUES ('128', 'MSB Bank S.A. Banco de Câmbio');
INSERT INTO tb_banco VALUES ('129', 'UBS Brasil Banco de Investimento S.A.');
INSERT INTO tb_banco VALUES ('184', 'Banco Itaú BBA S.A.');
INSERT INTO tb_banco VALUES ('204', 'Banco Bradesco Cartões S.A.');
INSERT INTO tb_banco VALUES ('208', 'Banco BTG Pactual S.A.');
INSERT INTO tb_banco VALUES ('212', 'Banco Original S.A.');
INSERT INTO tb_banco VALUES ('215', 'Banco Comercial e de Investimento Sudameris S.A.');
INSERT INTO tb_banco VALUES ('217', 'Banco John Deere S.A.');
INSERT INTO tb_banco VALUES ('218', 'Banco Bonsucesso S.A.');
INSERT INTO tb_banco VALUES ('222', 'Banco Credit Agricole Brasil S.A.');
INSERT INTO tb_banco VALUES ('224', 'Banco Fibra S.A.');
INSERT INTO tb_banco VALUES ('233', 'Banco Cifra S.A.');
INSERT INTO tb_banco VALUES ('237', 'Banco Bradesco S.A.');
INSERT INTO tb_banco VALUES ('246', 'Banco ABC Brasil S.A.');
INSERT INTO tb_banco VALUES ('248', 'Banco Boavista Interatlântico S.A.');
INSERT INTO tb_banco VALUES ('249', 'Banco Investcred Unibanco S.A.');
INSERT INTO tb_banco VALUES ('250', 'BCV - Banco de Crédito e Varejo S.A.');
INSERT INTO tb_banco VALUES ('254', 'Paraná Banco S.A.');
INSERT INTO tb_banco VALUES ('263', 'Banco Cacique S.A.');
INSERT INTO tb_banco VALUES ('265', 'Banco Fator S.A.');
INSERT INTO tb_banco VALUES ('318', 'Banco BMG S.A.');
INSERT INTO tb_banco VALUES ('320', 'China Construction Bank (Brasil) Banco Multiplo S.A.');
INSERT INTO tb_banco VALUES ('341', 'Itaú Unibanco S.A.');
INSERT INTO tb_banco VALUES ('356', 'Banco Real S.A.');
INSERT INTO tb_banco VALUES ('366', 'Banco Société Générale Brasil S.A.');
INSERT INTO tb_banco VALUES ('370', 'Banco Mizuho do Brasil S.A.');
INSERT INTO tb_banco VALUES ('376', 'Banco J. P. Morgan S.A.');
INSERT INTO tb_banco VALUES ('389', 'Banco Mercantil do Brasil S.A.');
INSERT INTO tb_banco VALUES ('394', 'Banco Bradesco Financiamentos S.A.');
INSERT INTO tb_banco VALUES ('399', 'HSBC Bank Brasil S.A. - Banco Múltiplo');
INSERT INTO tb_banco VALUES ('422', 'Banco Safra S.A.');
INSERT INTO tb_banco VALUES ('456', 'Banco de Tokyo-Mitsubishi UFJ Brasil S.A.');
INSERT INTO tb_banco VALUES ('464', 'Banco Sumitomo Mitsui Brasileiro S.A.');
INSERT INTO tb_banco VALUES ('473', 'Banco Caixa Geral - Brasil S.A.');
INSERT INTO tb_banco VALUES ('477', 'Citibank N.A.');
INSERT INTO tb_banco VALUES ('479', 'Banco ItaúBank S.A');
INSERT INTO tb_banco VALUES ('487', 'Deutsche Bank S.A. - Banco Alemão');
INSERT INTO tb_banco VALUES ('488', 'JPMorgan Chase Bank');
INSERT INTO tb_banco VALUES ('492', 'ING Bank N.V.');
INSERT INTO tb_banco VALUES ('505', 'Banco Credit Suisse (Brasil) S.A.');
INSERT INTO tb_banco VALUES ('600', 'Banco Luso Brasileiro S.A.');
INSERT INTO tb_banco VALUES ('604', 'Banco Industrial do Brasil S.A.');
INSERT INTO tb_banco VALUES ('610', 'Banco VR S.A.');
INSERT INTO tb_banco VALUES ('611', 'Banco Paulista S.A.');
INSERT INTO tb_banco VALUES ('612', 'Banco Guanabara S.A.');
INSERT INTO tb_banco VALUES ('623', 'Banco PAN S.A.');
INSERT INTO tb_banco VALUES ('626', 'Banco Ficsa S.A.');
INSERT INTO tb_banco VALUES ('633', 'Banco Rendimento S.A.');
INSERT INTO tb_banco VALUES ('634', 'Banco Triângulo S.A.');
INSERT INTO tb_banco VALUES ('641', 'Banco Alvorada S.A.');
INSERT INTO tb_banco VALUES ('643', 'Banco Pine S.A.');
INSERT INTO tb_banco VALUES ('652', 'Itaú Unibanco Holding S.A.');
INSERT INTO tb_banco VALUES ('653', 'Banco Indusval S.A.');
INSERT INTO tb_banco VALUES ('655', 'Banco Votorantim S.A.');
INSERT INTO tb_banco VALUES ('707', 'Banco Daycoval S.A.');
INSERT INTO tb_banco VALUES ('719', 'Banif-Banco Internacional do Funchal (Brasil)S.A.');
INSERT INTO tb_banco VALUES ('739', 'Banco Cetelem S.A.');
INSERT INTO tb_banco VALUES ('740', 'Banco Barclays S.A.');
INSERT INTO tb_banco VALUES ('745', 'Banco Citibank S.A.');
INSERT INTO tb_banco VALUES ('746', 'Banco Modal S.A.');
INSERT INTO tb_banco VALUES ('747', 'Banco Rabobank International Brasil S.A.');
INSERT INTO tb_banco VALUES ('748', 'Banco Cooperativo Sicredi S.A.');
INSERT INTO tb_banco VALUES ('751', 'Scotiabank Brasil S.A. Banco Múltiplo');
INSERT INTO tb_banco VALUES ('752', 'Banco BNP Paribas Brasil S.A.');
INSERT INTO tb_banco VALUES ('755', 'Bank of America Merrill Lynch Banco Múltiplo S.A.');
INSERT INTO tb_banco VALUES ('756', 'Banco Cooperativo do Brasil S.A. - BANCOOB');