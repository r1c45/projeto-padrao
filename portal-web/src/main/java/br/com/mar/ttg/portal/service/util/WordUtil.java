package br.com.mar.ttg.portal.service.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Section;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author ggraf
 *
 */
public class WordUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordUtil.class);

	public static void substituiValores(HWPFDocument doc, Map<String, String> mapaValores) throws IOException {
		Range r = doc.getRange();

		for (int i = 0; i < r.numSections(); ++i) {
			Section s = r.getSection(i);

			for (int j = 0; j < s.numParagraphs(); j++) {
				Paragraph p = s.getParagraph(j);

				for (int k = 0; k < p.numCharacterRuns(); k++) {
					CharacterRun run = p.getCharacterRun(k);
					String text = run.text();

					for (String chave : mapaValores.keySet()) {
						if (text.contains(chave)) {
							String valor = mapaValores.get(chave);
							run.replaceText(chave, valor);
						}
					}
				}
			}
		}
	}

	public static void substituiValores(XWPFDocument doc, Map<String, String> mapaValores) {
		Iterator<XWPFParagraph> paragrafos = doc.getParagraphsIterator();

		while (paragrafos.hasNext()) {
			XWPFParagraph paragrafo = (XWPFParagraph) paragrafos.next();
			List<XWPFRun> runs = paragrafo.getRuns();

			for (int i = 0; i < runs.size(); i++) {
				String runText = runs.get(i).getText(runs.get(i).getTextPosition());

				if (StringUtils.isNotEmpty(runText) && mapaValores.containsKey(runText)) {
					runText = mapaValores.get(runText);
					runs.get(i).setText(runText, 0);
				}
			}
		}

		Iterator<XWPFTable> tabelas = doc.getTablesIterator();

		while (tabelas.hasNext()) {
			XWPFTable tabela = (XWPFTable) tabelas.next();
			int linhasTabela = tabela.getNumberOfRows();

			for (int i = 0; i < linhasTabela; i++) {
				XWPFTableRow row = tabela.getRow(i);
				List<XWPFTableCell> cells = row.getTableCells();

				for (XWPFTableCell cell : cells) {
					String runText = cell.getText();

					for (Entry<String, String> entry : mapaValores.entrySet()) {
						String chave = entry.getKey();

						if (runText.contains(chave)) {
							String valor = StringUtils.defaultString(entry.getValue());

							LOGGER.debug("CHAVE - " + chave);
							LOGGER.debug("TEXTO ANTES - " + runText);
							runText = runText.replace(chave, StringUtils.defaultString(valor));
							LOGGER.debug("TEXTO DEPOIS - " + runText);
						}
					}
					cell.setText(runText);
				}
			}
		}
	}
}