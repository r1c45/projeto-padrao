package br.com.mar.ttg.portal.service.impl.auxiliar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.auxiliar.Banco;
import br.com.mar.ttg.portal.data.impl.auxiliar.BancoRepository;
import br.com.mar.ttg.portal.service.exception.AlreadyExistsException;
import br.com.mar.ttg.portal.service.exception.NoResultFoundException;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;

/**
 * 
 * @author ggraf
 *
 */
@Service
public class BancoService extends CachedService {

	private static final String CACHE_NAME = "bancos";

	private static final String CACHE_KEY = "all";

	@Autowired
	private BancoRepository repository;

	@SuppressWarnings("unchecked")
	public List<Banco> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

		if (null == cacheObj) {
			List<Banco> objetos = repository.consulta(new Banco());
			putOnCache(CACHE_NAME, CACHE_KEY, ObjectCopier.copy(objetos));

			cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

			return objetos;

		}

		return (List<Banco>) ObjectCopier.copy(cacheObj);
	}

	public List<Banco> consulta(Banco entity) {
		return repository.consulta(entity);
	}

	public Banco consultaPorCodigo(String codigo) {
		Banco entity = repository.findById(codigo);

		if (null == entity) {
			throw new NoResultFoundException();
		}

		return entity;
	}

	public void cadastra(Banco entity) {
		try {
			consultaPorCodigo(entity.getCodigo());

			throw new AlreadyExistsException();

		} catch (NoResultFoundException e) {

		}

		repository.store(entity);

		clearCache(CACHE_NAME);
	}

	public void atualiza(Banco entity) {
		repository.update(entity);

		clearCache(CACHE_NAME);
	}
}