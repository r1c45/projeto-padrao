package br.com.mar.ttg.portal.web.util;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.web.security.UserInfo;

/**
 * 
 * @author ggraf
 *
 */
public class SessionUtil {

	public static Usuario getAuthenticatedUsuario() {
		UserInfo userInfo = getAuthenticatedUser();

		if (null != userInfo) {
			return userInfo.getUsuario();
		}

		return null;
	}

	public static UserInfo getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (null != authentication && authentication instanceof UsernamePasswordAuthenticationToken) {
			UserInfo user = (UserInfo) ((UsernamePasswordAuthenticationToken) authentication).getPrincipal();
			return user;
		} else {
			return null;
		}
	}

	public static boolean possuiPermissao(String permissao) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		return authentication.getAuthorities().contains(new SimpleGrantedAuthority(permissao));
	}

	public static String getIdLogin() {
		return getAuthenticatedUser().getIdLogin();
	}
}