package br.com.mar.ttg.portal.web.controller.acesso;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.data.entity.acesso.PerfilUsuario;
import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioColaborador;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioExterno;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioSistema;
import br.com.mar.ttg.portal.data.entity.auxiliar.Departamento;
import br.com.mar.ttg.portal.data.entity.auxiliar.NivelHierarquia;
import br.com.mar.ttg.portal.dom.DomAcesso.DomEventoLogin;
import br.com.mar.ttg.portal.dom.DomAcesso.DomStatusUsuario;
import br.com.mar.ttg.portal.service.exception.AlreadyExistsException;
import br.com.mar.ttg.portal.service.exception.AvisoException;
import br.com.mar.ttg.portal.service.exception.ValidacaoException;
import br.com.mar.ttg.portal.service.impl.acesso.AuditoriaService;
import br.com.mar.ttg.portal.service.impl.acesso.PerfilUsuarioService;
import br.com.mar.ttg.portal.service.impl.acesso.UsuarioService;
import br.com.mar.ttg.portal.service.impl.auxiliar.DepartamentoService;
import br.com.mar.ttg.portal.service.impl.auxiliar.NivelHierarquiaService;
import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.web.util.I18NUtil;
import br.com.mar.ttg.portal.web.util.SessionUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
public class UserController extends CrudBaseController<Usuario, Usuario> implements InitializingBean {

	private static final long serialVersionUID = -2744886475383699841L;

	private static final String VIEW_ALTERACAO_SENHA = "usuario/acesso/alteracao_senha.xhtml";

	private static final String VIEW_USUARIO_SISTEMA = "administracao/acesso/usuario/sistema.xhtml";

	private static final String VIEW_USUARIO_EXTERNO = "administracao/acesso/usuario/externo.xhtml";

	private static final String VIEW_CONTATOS = "pessoas/contatos.xhtml";

	private static final String VIEW_USUARIO_COLABORADOR = "administracao/acesso/usuario/colaborador.jsf";

	@Autowired
	private UsuarioService service;

	@Autowired
	private PerfilUsuarioService perfilService;

	@Autowired
	private DepartamentoService departamentoService;

	@Autowired
	private NivelHierarquiaService nivelHierarquiaService;

	@Autowired
	private AuditoriaService auditoriaService;

	private List<PerfilUsuario> perfis;

	private List<Departamento> departamentos;

	private List<NivelHierarquia> niveisHierarquia;

	private List<Usuario> gestores;

	private String senhaAtual;

	private String novaSenha;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			if (isView(VIEW_ALTERACAO_SENHA)) {
				setEntity(SessionUtil.getAuthenticatedUsuario());

		} else if (isView(VIEW_USUARIO_COLABORADOR)) {
				setFilterEntity(new UsuarioColaborador());

				perfis = (perfilService.consultaPerfil());

				setDepartamentos(departamentoService.consulta());

				setNiveisHierarquia(nivelHierarquiaService.consulta());

			} else if (isView(VIEW_USUARIO_SISTEMA)) {
				setFilterEntity(new UsuarioSistema());

				setPerfis(perfilService.consulta());

			} else if (isView(VIEW_CONTATOS)) {
				setFilterEntity(new UsuarioColaborador());
				getFilterEntity().setStatus(DomStatusUsuario.ATIVO);

			} else if (isView(VIEW_USUARIO_EXTERNO)) {
				setFilterEntity(new UsuarioExterno());

				setPerfis(perfilService.consulta());

			}

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}

	}

	@Override
	protected boolean executeSave() {
		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_usuario_atualizado"), null);

		} else {
			try {
				service.cadastra(getEntity());

				returnInfoMessage(I18NUtil.getMessage("sucesso_usuario_cadastrado"), null);

			} catch (AvisoException e) {
				returnWarnDialogMessage(I18NUtil.getMessage("aviso"), e.getMessage(), null);

			} catch (AlreadyExistsException e) {
				returnWarnDialogMessage(I18NUtil.getMessage("aviso"), I18NUtil.getMessage("erro_usuario_cadastrado"),
						null);
				return false;
			}
		}

		executeEdit(getEntity());

		return true;
	}

	@Override
	protected void beforeAdd() {
		if (isView(VIEW_USUARIO_COLABORADOR)) {
			setEntity(new UsuarioColaborador());

			gestores = service.consulta(new UsuarioColaborador());

		} else if (isView(VIEW_USUARIO_SISTEMA)) {
			setEntity(new UsuarioSistema());

		} else if (isView(VIEW_USUARIO_EXTERNO)) {
			setEntity(new UsuarioExterno());
		}
	}

	@Override
	protected void executeEdit(Usuario entity) {
		setEntity(entity);
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consultaUsuario(getFilterEntity()));

	}

	public void cadastraAvaliacoes() {
		try {
			returnInfoDialogMessage(I18NUtil.getMessage("sucesso"), I18NUtil.getMessage("sucesso_cadastro_avaliacoes"));

			executeEdit(getEntity());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_cadastro_avaliacoes"), t);
		}
	}

	public void reiniciaSenha() {
		try {
			service.reiniciaSenha(getEntity());

			returnInfoDialogMessage(I18NUtil.getMessage("sucesso"),
					I18NUtil.getMessage("sucesso_senha_reinicializacao"));

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_senha_reinicializacao"), t);

		}
	}

	public void alteraSenha() {
		try {
			// service.alteraSenha(getEntity().getId(), senhaAtual, novaSenha);

			returnInfoDialogMessage(I18NUtil.getMessage("sucesso"), I18NUtil.getMessage("sucesso_senha_alteracao"));

			auditoriaService.registraEventoLogin(SessionUtil.getIdLogin(), DomEventoLogin.TROCA_SENHA, null);

		} catch (ValidacaoException e) {
			returnWarnDialogMessage(I18NUtil.getMessage("aviso"), e.getMessage(), null);

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_senha_alteracao"), t);

		} finally {
			novaSenha = null;
			senhaAtual = null;
		}
	}

	@Override
	protected void select(Usuario entity) {
		// TODO Auto-generated method stub

	}

	public List<PerfilUsuario> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<PerfilUsuario> perfis) {
		this.perfis = perfis;
	}

	public List<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(List<Departamento> departamentos) {
		this.departamentos = departamentos;
	}

	public List<NivelHierarquia> getNiveisHierarquia() {
		return niveisHierarquia;
	}

	public void setNiveisHierarquia(List<NivelHierarquia> niveisHierarquia) {
		this.niveisHierarquia = niveisHierarquia;
	}

	public List<Usuario> getGestores() {
		return gestores;
	}

	public void setGestores(List<Usuario> gestores) {
		this.gestores = gestores;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
}