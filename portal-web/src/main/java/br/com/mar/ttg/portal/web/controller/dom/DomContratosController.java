package br.com.mar.ttg.portal.web.controller.dom;

import java.io.Serializable;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.dom.DomContratos;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@Scope("view")
public class DomContratosController implements Serializable {

	private static final long serialVersionUID = -7214605828159917494L;

	public Map<String, Object> getStatusTipoMap() {
		return DomContratos.domStatusTipo.getMap();
	}

	public Map<String, Object> getTipoRequisicaoMap() {
		return DomContratos.domTipoRequisicao.getMap();
	}

	public Map<String, Object> getModeloRequisicaoMap() {
		return DomContratos.domModeloRequisicao.getMap();
	}

	public Map<String, Object> getEtapaRequisicaoMap() {
		return DomContratos.domEtapaRequisicao.getMap();
	}

	public Map<String, Object> getStatusRequisicaoMap() {
		return DomContratos.domStatusRequisicao.getMap();
	}

	public Map<String, Object> getIndiceReajusteMap() {
		return DomContratos.domIndiceReajuste.getMap();
	}

	public Map<String, Object> getAreaAtuacaoMap() {
		return DomContratos.domAreaAtuacao.getMap();
	}

	public Map<String, Object> getTipoAssinaturaMap() {
		return DomContratos.domTipoAssinatura.getMap();
	}

}