package br.com.mar.ttg.portal.data.impl.acesso;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.data.entity.acesso.PerfilUsuario;
import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.service.util.BaseRepository;

/**
 * 
 * @author ggraf
 *
 */
@Repository
public class PerfilUsuarioRepository extends BaseRepository<PerfilUsuario> {

	public PerfilUsuarioRepository() {
		super(PerfilUsuario.class);
	}

	public List<PerfilUsuario> consulta(PerfilUsuario entity) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<PerfilUsuario> query = builder.createQuery(PerfilUsuario.class);
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<PerfilUsuario> table = query.from(PerfilUsuario.class);

		if (StringUtils.isNotEmpty(entity.getNome())) {
			predicateList.add(builder.and(builder.like(builder.upper(table.<String> get("nome")), "%" + entity.getNome().toUpperCase() + "%")));
		}

		query.orderBy(builder.asc(table.get("nome")));
		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<PerfilUsuario> typedQuery = getEntityManager().createQuery(query);

		return (List<PerfilUsuario>) typedQuery.getResultList();
	}
	
	public List<PerfilUsuario> consultaPerfil(){
		String queryStr = "SELECT * FROM tb_perfil_usuario";
		
		Query query = getEntityManager().createNativeQuery(queryStr);

		return (List<PerfilUsuario>) query.getResultList();
	}
	
}