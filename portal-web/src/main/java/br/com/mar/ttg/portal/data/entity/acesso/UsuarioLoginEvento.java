package br.com.mar.ttg.portal.data.entity.acesso;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ggraf
 *
 */
@Entity
@Table(name = "tb_usuario_login_evento")
public class UsuarioLoginEvento implements Serializable {

	private static final long serialVersionUID = -7038235125263693436L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "id_login", nullable = false)
	private String idLogin;

	@Column(name = "data", nullable = false)
	private Date data;

	@Column(name = "tipo")
	private String tipo;

	@Column(name = "conteudo")
	private String conteudo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(String idLogin) {
		this.idLogin = idLogin;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
}