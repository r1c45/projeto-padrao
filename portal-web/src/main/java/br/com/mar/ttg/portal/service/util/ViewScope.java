package br.com.mar.ttg.portal.service.util;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.naming.spi.ObjectFactory;

import org.springframework.beans.factory.config.Scope;

public class ViewScope implements Scope {

	public Object get(String name, @SuppressWarnings("rawtypes") ObjectFactory objectFactory) {
		Map<String, Object> viewMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();

		if (viewMap.containsKey(name)) {
			return viewMap.get(name);
		} else {
			Object object = ((org.springframework.beans.factory.ObjectFactory<?>) objectFactory).getObject();
			viewMap.put(name, object);

			return object;
		}
	}

	@Override
	public Object remove(String name) {
		return FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(name);
	}

	@Override
	public String getConversationId() {
		return null;
	}

	@Override
	public void registerDestructionCallback(String name, Runnable callback) {
	}

	@Override
	public Object resolveContextualObject(String key) {
		return null;
	}

	@Override
	public Object get(String arg0, org.springframework.beans.factory.ObjectFactory<?> arg1) {
		// TODO Auto-generated method stub
		return null;
	}
}
