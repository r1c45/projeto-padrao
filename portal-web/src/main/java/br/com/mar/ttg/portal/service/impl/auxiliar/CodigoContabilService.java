package br.com.mar.ttg.portal.service.impl.auxiliar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.auxiliar.CodigoContabil;
import br.com.mar.ttg.portal.data.impl.auxiliar.CodigoContabilRepository;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class CodigoContabilService extends CachedService {

	private static final String CACHE_NAME = "codigosContabeis";

	private static final String CACHE_KEY = "all";

	@Autowired
	private CodigoContabilRepository repository;

	@SuppressWarnings("unchecked")
	public List<CodigoContabil> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

		if (null == cacheObj) {
			List<CodigoContabil> objetos = repository.consulta(new CodigoContabil());
			putOnCache(CACHE_NAME, CACHE_KEY, ObjectCopier.copy(objetos));

			cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

			return objetos;

		}

		return (List<CodigoContabil>) ObjectCopier.copy(cacheObj);
	}

	public List<CodigoContabil> consulta(CodigoContabil entity) {
		return repository.consulta(entity);
	}

	public void cadastra(CodigoContabil entity) {
		repository.store(entity);

		clearCache(CACHE_NAME);
	}

	public void atualiza(CodigoContabil entity) {
		repository.update(entity);

		clearCache(CACHE_NAME);
	}
}