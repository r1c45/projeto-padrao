package br.com.mar.ttg.portal.service.exception;

/**
 * 
 * @author ggraf
 *
 */
public class NoResultFoundException extends RuntimeException {

	private static final long serialVersionUID = 9068863370591892317L;

	public NoResultFoundException() {
		super();
	}
}