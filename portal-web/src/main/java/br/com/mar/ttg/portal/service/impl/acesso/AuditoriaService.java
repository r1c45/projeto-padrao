package br.com.mar.ttg.portal.service.impl.acesso;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.acesso.UsuarioLogin;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioLoginEvento;
import br.com.mar.ttg.portal.data.impl.acesso.UsuarioLoginEventoRepository;
import br.com.mar.ttg.portal.data.impl.acesso.UsuarioLoginRepository;

/**
 * 
 * @author ggraf
 *
 */
@Service
public class AuditoriaService {

	@Autowired
	private UsuarioLoginRepository loginRepository;

	@Autowired
	private UsuarioLoginEventoRepository loginEventoRepository;

	public String registraLogin(Integer idUsuario, String ipOrigem) {
		UsuarioLogin login = new UsuarioLogin();
		login.setId(UUID.randomUUID().toString());
		login.setData(new Date());
		login.setIdUsuario(idUsuario);
		login.setIpOrigem(ipOrigem);

		loginRepository.store(login);

		return login.getId();
	}

	public void registraEventoLogin(String idLogin, String tipo, String conteudo) {
		UsuarioLoginEvento evento = new UsuarioLoginEvento();
		evento.setId(UUID.randomUUID().toString());
		evento.setIdLogin(idLogin);
		evento.setData(new Date());
		evento.setTipo(tipo);
		evento.setConteudo(conteudo);

		loginEventoRepository.store(evento);
	}
}