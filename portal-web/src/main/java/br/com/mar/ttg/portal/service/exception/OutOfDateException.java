package br.com.mar.ttg.portal.service.exception;

/**
 * 
 * @author ggraf
 *
 */
public class OutOfDateException extends RuntimeException {

	private static final long serialVersionUID = -1104339491797768171L;

	public OutOfDateException() {
		super();
	}
}