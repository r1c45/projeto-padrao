package br.com.mar.ttg.portal.web.controller.auxiliar;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.data.entity.auxiliar.CodigoContabil;
import br.com.mar.ttg.portal.service.impl.auxiliar.CodigoContabilService;
import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.web.util.I18NUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
public class CodigoContabilController extends CrudBaseController<CodigoContabil, CodigoContabil> implements InitializingBean {

	private static final long serialVersionUID = 9148285670278075062L;

	@Autowired
	private CodigoContabilService service;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			setFilterEntity(new CodigoContabil());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	@Override
	protected boolean executeSave() {
		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_codigo_atualizado"), null);

		} else {
			service.cadastra(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_codigo_cadastrado"), null);
		}

		return true;
	}

	@Override
	protected void beforeAdd() {
		setEntity(new CodigoContabil());
	}

	@Override
	protected void executeEdit(CodigoContabil entity) {
		setEntity(entity);
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consulta(getFilterEntity()));
	}
}