package br.com.mar.ttg.portal.service.exception;

/**
 * 
 * @author ggraf
 *
 */
public class AlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = -5667128938099127705L;

	public AlreadyExistsException() {
		super();
	}
}