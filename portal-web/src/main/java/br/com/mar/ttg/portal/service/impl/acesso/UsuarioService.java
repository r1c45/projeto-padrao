package br.com.mar.ttg.portal.service.impl.acesso;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioColaborador;
import br.com.mar.ttg.portal.data.impl.acesso.UsuarioRepository;
import br.com.mar.ttg.portal.dom.DomAcesso.DomStatusUsuario;
import br.com.mar.ttg.portal.enums.ConfiguracaoEnum;
import br.com.mar.ttg.portal.service.exception.AlreadyExistsException;
import br.com.mar.ttg.portal.service.exception.AvisoException;
import br.com.mar.ttg.portal.service.exception.NoResultFoundException;
import br.com.mar.ttg.portal.service.impl.auxiliar.ConfiguracaoService;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.EncryptHelper;
import br.com.mar.ttg.portal.service.util.ObjectCopier;

/**
 * 
 * @author ggraf
 *
 */
@Service
public class UsuarioService extends CachedService {

	private static final String CACHE_NAME = "usuarios";

	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private ConfiguracaoService configuracaoService;

	@Transactional(rollbackFor = Throwable.class, noRollbackFor = AvisoException.class)
	public void cadastra(Usuario entity) {
		String senha = configuracaoService.consulta(ConfiguracaoEnum.USUARIO_SENHA_PADRAO);

		entity.setSenha(EncryptHelper.encrypt(senha, senha));

		try {
			consultaPorEmail(entity.getEmail());

			throw new AlreadyExistsException();

		} catch (NoResultFoundException e) {

		}

		preparaGravacao(entity);

		repository.store(entity);

		// Caso seja colaborador, cadastra nos processos em aberto
		if (entity instanceof UsuarioColaborador) {
		}
	}

	public void atualiza(Usuario entity) {
		preparaGravacao(entity);

		repository.update(entity);

		putOnCache(CACHE_NAME, entity.getId().toString(), null);
	}

	private void preparaGravacao(Usuario entity) {
		if (entity instanceof UsuarioColaborador) {
			if (null != ((UsuarioColaborador) entity).getIdDepartamento() && 0 == ((UsuarioColaborador) entity).getIdDepartamento()) {
				((UsuarioColaborador) entity).setIdDepartamento(null);
			}

			if (null != ((UsuarioColaborador) entity).getIdNivelHierarquia() && 0 == ((UsuarioColaborador) entity).getIdNivelHierarquia()) {
				((UsuarioColaborador) entity).setIdNivelHierarquia(null);
			}

			if (null != ((UsuarioColaborador) entity).getIdGestor() && 0 == ((UsuarioColaborador) entity).getIdGestor()) {
				((UsuarioColaborador) entity).setIdGestor(null);
			}
		}
	}

	public List<Usuario> consulta(Usuario entity) {
		return repository.consulta(entity);
	}
	
	public List<Usuario> consultaUsuario(Usuario usuario) {
		return repository.consulta(usuario);
	}

	public List<Usuario> consultaPorPermissao(String permissao) {
		return repository.consultaPorPermissao(permissao);
	}

	public boolean possuiPermissao(Integer idUsuario, String permissao) {
		return repository.possuiPermissao(idUsuario, permissao);
	}

	public List<Usuario> consultaColaboradoresAtivosPorHierarquia(Integer idNivelHierarquia) {
		UsuarioColaborador filter = new UsuarioColaborador();
		filter.setStatus(DomStatusUsuario.ATIVO);
		filter.setIdNivelHierarquia(idNivelHierarquia);

		return consulta(filter);
	}

	public List<Usuario> consultaColaboradoresAtivosPorGestor(Integer idGestor) {
		UsuarioColaborador filter = new UsuarioColaborador();
		filter.setStatus(DomStatusUsuario.ATIVO);
		filter.setIdGestor(idGestor);

		return consulta(filter);
	}

	public List<Usuario> consultaColaboradoresAtivos() {
		UsuarioColaborador filter = new UsuarioColaborador();
		filter.setStatus(DomStatusUsuario.ATIVO);

		return consulta(filter);
	}

	public Usuario consultaPorId(Integer id) {
		Object cacheObj = findOnCache(CACHE_NAME, id.toString());

		if (null == cacheObj) {
			Usuario entity = repository.findById(id);

			if (null == entity) {
				throw new NoResultFoundException();
			}

			putOnCache(CACHE_NAME, id.toString(), ObjectCopier.copy(entity));

			cacheObj = findOnCache(CACHE_NAME, id.toString());

			return (Usuario) cacheObj;

		} else {
			return (Usuario) ObjectCopier.copy(cacheObj);

		}
	}

	public Usuario consultaPorEmail(String codigo) {
		try {
			return repository.consultaPorEmail(codigo);

		} catch (NoResultException e) {
			throw new NoResultFoundException();
		}
	}

	public List<String> consultaEmailsDepartamento(String permissaoDepartamento) {
		List<Usuario> usuarios = consultaPorPermissao(permissaoDepartamento);

		List<String> emails = new ArrayList<>();

		for (Usuario usuario : usuarios) {
			emails.add(usuario.getEmail());
		}

		return emails;
	}

	public void reiniciaSenha(Usuario entity) {
		String senha = configuracaoService.consulta(ConfiguracaoEnum.USUARIO_SENHA_PADRAO);

		//entity.setSenha(EncryptHelper.encrypt(senha));

		repository.update(entity);
	}
}