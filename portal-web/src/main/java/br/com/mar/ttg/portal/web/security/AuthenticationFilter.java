package br.com.mar.ttg.portal.web.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.TextEscapeUtils;

import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioColaborador;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioExterno;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioSistema;
import br.com.mar.ttg.portal.dom.DomAcesso.DomPermissao;
import br.com.mar.ttg.portal.service.impl.acesso.AuditoriaService;
import br.com.mar.ttg.portal.service.impl.acesso.UsuarioService;

public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	private static final String SPRING_SECURITY_FORM_USERNAME_KEY = "j_username";
	private static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "j_password";

	public static final String SPRING_SECURITY_LAST_USERNAME_KEY = "SPRING_SECURITY_LAST_USERNAME";

	private boolean postOnly = true;

	private static Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private AuditoriaService auditoriaService;

	public AuthenticationFilter() {
		super("/j_spring_security_check");
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		try {
			if (postOnly && !request.getMethod().equals("POST")) {
				throw new AuthenticationServiceException("");
			}

			String email = request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
			String senha = request.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY);
			String ipOrigem = request.getRemoteAddr();

			if (StringUtils.isEmpty(email)) {
				throw new AuthenticationServiceException("Favor informar o email");
			}

			if (StringUtils.isEmpty(senha)) {
				throw new AuthenticationServiceException("Favor informar a senha");
			}

			UsernamePasswordAuthenticationToken autenticacao = new UsernamePasswordAuthenticationToken(email, senha);

			HttpSession session = request.getSession(false);

			if (session != null || getAllowSessionCreation()) {
				request.getSession().setAttribute(SPRING_SECURITY_LAST_USERNAME_KEY, TextEscapeUtils.escapeEntities(email));
			}

			autenticacao.setDetails(authenticationDetailsSource.buildDetails(request));

			// Autentica o usuario
			autenticacao = (UsernamePasswordAuthenticationToken) this.getAuthenticationManager().authenticate(autenticacao);

			// Consulta o usuario	
			Usuario usuario = usuarioService.consultaPorEmail(email);

			// Prepara o objeto autenticado
			UserInfo principal = new UserInfo(email, "", autenticacao.getAuthorities(), usuario);

			List<GrantedAuthority> authorities = new ArrayList<>();
			authorities.addAll(autenticacao.getAuthorities());

			if (usuario instanceof UsuarioSistema) {
				authorities.add(new SimpleGrantedAuthority(DomPermissao.USUARIO_SISTEMA));

			} else if (usuario instanceof UsuarioColaborador) {
				authorities.add(new SimpleGrantedAuthority(DomPermissao.USUARIO_COLABORADOR));

			} else if (usuario instanceof UsuarioExterno) {
				authorities.add(new SimpleGrantedAuthority(DomPermissao.USUARIO_EXTERNO));
			}

			autenticacao = new UsernamePasswordAuthenticationToken(principal, autenticacao.getCredentials(), authorities);

			String idLogin = auditoriaService.registraLogin(usuario.getId(), ipOrigem);
			principal.setIdLogin(idLogin);

			return autenticacao;

		} catch (BadCredentialsException e) {
			throw new AuthenticationServiceException("E-mail e/ou senha incorreto(s)");

		} catch (DisabledException e) {
			throw new AuthenticationServiceException("Usuário com acesso desabilitado");

		} catch (AuthenticationServiceException e) {
			throw e;

		} catch (Throwable t) {
			LOGGER.error("Erro ao autenticar usuario", t);
			throw new AuthenticationServiceException("Erro ao realizar a autenticação, contate o administrador");
		}
	}
}