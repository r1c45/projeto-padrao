package br.com.mar.ttg.portal.web.controller.dom;

import java.io.Serializable;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.dom.DomCadastro;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@Scope("view")
public class DomCadastroController implements Serializable {

	private static final long serialVersionUID = -7380704564971395576L;

	public Map<String, Object> getStatusParceiroMap() {
		return DomCadastro.domStatusParceiro.getMap();
	}
}