package br.com.mar.ttg.portal.service.impl.auxiliar;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.auxiliar.Estado;
import br.com.mar.ttg.portal.data.impl.auxiliar.EstadoRepository;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class EstadoService extends CachedService {

	private static final String CACHE_NAME = "estados";
	private static final String ALL_KEY = "all";

	@Autowired
	private EstadoRepository repository;

	@SuppressWarnings("unchecked")
	public List<Estado> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, ALL_KEY);
		
		if (null == cacheObj) {
			List<Estado> objetos = repository.consulta();
			putOnCache(CACHE_NAME, ALL_KEY, ObjectCopier.copy(objetos));
			return objetos;
		
		} else {
			List<Estado> objetos = new ArrayList<Estado>();
			objetos.addAll((List<Estado>) cacheObj);
			return objetos;
			
		}
	}
}