package br.com.mar.ttg.portal.data.entity.auxiliar;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author ggraf
 *
 */
@Entity
@Table(name = "tb_codigo_contabil")
public class CodigoContabil implements Serializable {

	private static final long serialVersionUID = 7075780107465526690L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_CODIGO_CONTABIL")
	@SequenceGenerator(name = "SQ_CODIGO_CONTABIL", sequenceName = "SQ_CODIGO_CONTABIL", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Integer id;

	@Column(name = "codigo", nullable = false)
	private String codigo;

	@Column(name = "ds_operacao", nullable = false)
	private String descricaoOperacao;

	@Column(name = "ds_operacao_en", nullable = false)
	private String descricaoOperacaoEn;

	@Column(name = "ds_atividade", nullable = false)
	private String descricaoAtividade;

	@Column(name = "unidade_medida", nullable = false)
	private String unidadeMedida;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricaoOperacao() {
		return descricaoOperacao;
	}

	public void setDescricaoOperacao(String descricaoOperacao) {
		this.descricaoOperacao = descricaoOperacao;
	}

	public String getDescricaoOperacaoEn() {
		return descricaoOperacaoEn;
	}

	public void setDescricaoOperacaoEn(String descricaoOperacaoEn) {
		this.descricaoOperacaoEn = descricaoOperacaoEn;
	}

	public String getDescricaoAtividade() {
		return descricaoAtividade;
	}

	public void setDescricaoAtividade(String descricaoAtividade) {
		this.descricaoAtividade = descricaoAtividade;
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
}