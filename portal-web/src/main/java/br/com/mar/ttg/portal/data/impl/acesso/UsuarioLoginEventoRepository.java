package br.com.mar.ttg.portal.data.impl.acesso;

import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioLoginEvento;

/**
 * @author ggraf
 *
 */
@Repository
public class UsuarioLoginEventoRepository extends BaseRepository<UsuarioLoginEvento> {

	public UsuarioLoginEventoRepository() {
		super(UsuarioLoginEvento.class);
	}
}