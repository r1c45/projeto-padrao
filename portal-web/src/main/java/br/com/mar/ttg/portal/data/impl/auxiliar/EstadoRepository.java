package br.com.mar.ttg.portal.data.impl.auxiliar;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.auxiliar.Estado;

/**
 * 
 * @author Guilherme
 * 
 */
@Repository
public class EstadoRepository extends BaseRepository<Estado> {

	public EstadoRepository() {
		super(Estado.class);
	}

	@SuppressWarnings("unchecked")
	public List<Estado> consulta() {
		Query query = getEntityManager().createNativeQuery("SELECT * FROM tb_estado ORDER BY nome", Estado.class);

		return query.getResultList();
	}
}