package br.com.mar.ttg.portal.service.util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Guilherme
 *
 * @param <S> Tipo do objeto para consulta
 * @param <E> Tipo do objeto para edicao
 */
public abstract class SearchBaseController<S, E> extends BaseController {

	private static final long serialVersionUID = -5060220802378146006L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchBaseController.class);

	private int tabIndex;

	private E entity;
	private S filterEntity;
	private List<S> entities;

	public void search() {
		try {
			if (null != entities) {
				entities.clear();
			}
			LOGGER.debug("Consultar");
			executeSearch();
		} catch (Throwable t) {
			returnFatalMessage("Erro ao consultar", null, t);
		}
	}

	protected abstract void executeSearch();

	protected abstract void select(S entity);

	public void close() {
		setEntity(null);
	}

	// Getters and setters
	public E getEntity() {
		return entity;
	}

	public void setEntity(E entity) {
		this.entity = entity;
	}

	public void setSelectedEntity(S entity) {
		tabIndex = 0;
		select(entity);
	}

	public S getFilterEntity() {
		return filterEntity;
	}

	public void setFilterEntity(S filterEntity) {
		this.filterEntity = filterEntity;
	}

	public List<S> getEntities() {
		return entities;
	}

	public void setEntities(List<S> entities) {
		this.entities = entities;
	}

	public int getTabIndex() {
		return tabIndex;
	}

	public void setTabIndex(int tabIndex) {
		this.tabIndex = tabIndex;
	}
}
