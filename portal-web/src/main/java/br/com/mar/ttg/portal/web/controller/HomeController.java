
package br.com.mar.ttg.portal.web.controller;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.service.util.BaseController;
import br.com.mar.ttg.portal.web.util.I18NUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
public class HomeController extends BaseController implements InitializingBean {

	private static final long serialVersionUID = -5813915838504671519L;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {


		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	// Getters e Setters

}