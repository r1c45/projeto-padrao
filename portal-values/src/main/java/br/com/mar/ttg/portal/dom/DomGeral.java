package br.com.mar.ttg.portal.dom;

/**
 * 
 * @author ggraf
 *
 */
public class DomGeral {

	public static class DomBoolean extends DomBase {
		public static final String SIM = "S";
		public static final String NAO = "N";

		DomBoolean() {
			mapa.put(SIM, "Sim");
			mapa.put(NAO, "Não");
		}
	}

	public static DomBoolean domBoolean = new DomBoolean();
}