package br.com.mar.ttg.portal.enums;

/**
 * 
 * @author ggraf
 *
 */
public enum ConfiguracaoEnum {

	USUARIO_SENHA_PADRAO("usuario.senha_padrao"),

	PROCESSO_AVALIACAO_QUANTIDADE_ITENS_COMPETENCIA(
			"processo_avaliacao.quantidade_itens_competencia"), PROCESSO_AVALIACAO_QUANTIDADE_ITENS_DESEMPENHO(
					"processo_avaliacao.quantidade_itens_desempenho"), PROCESSO_AVALIACAO_DESTINATARIO_RELATORIO(
							"processo_avaliacao.destinatario_relatorio"), PROCESSO_AVALIACAO_DIRETORIO_ARQUIVOS(
									"processo_avaliacao.diretorio_arquivos"),

	PROCESSO_AVALIACAO_ETAPA_PREPARACAO_EMAIL("processo_avaliacao.etapa_pre.email"), PROCESSO_AVALIACAO_ETAPA_FASE_1_EMAIL(
			"processo_avaliacao.etapa_fa1.email"), PROCESSO_AVALIACAO_ETAPA_FASE_2_EMAIL(
					"processo_avaliacao.etapa_fa2.email"), PROCESSO_AVALIACAO_ETAPA_FASE_3_EMAIL(
							"processo_avaliacao.etapa_fa3.email"), PROCESSO_AVALIACAO_ETAPA_FINALIZACAO_EMAIL("processo_avaliacao.etapa_fnl.email"),

	EMAIL_SERVIDOR("email.servidor"), EMAIL_PORTA("email.porta"), EMAIL_USUARIO("email.usuario"), EMAIL_SENHA("email.senha"), EMAIL_SSL("email.ssl"),

	TIPO_CONTRATO_DIRETORIO_MINUTAS("tipo_contrato.diretorio_minutas"),

	REQUISICAO_CONTRATO_DIRETORIO_ARQUIVOS("requisicao_contrato.diretorio_arquivos"), REQUISICAO_CONTRATO_DIRETORIO_HISTORICO(
			"requisicao_contrato.diretorio_historico"), REQUISICAO_CONTRATO_URL_DOCUMENTOS(
					"requisicao_contrato.url_documentos"), REQUISICAO_CONTRATO_DIRETORIO_RELATORIO_MENSAL_GESTOR(
							"requisicao_contrato.diretorio_relatorio_mensal_gestor"),

	REQUISICAO_CONTRATO_RELATORIO_MENSAL_GESTOR_EMAIL("requisicao_contrato.relatorio_mensal_gestor.email"),

	REQUISICAO_CONTRATO_ETAPA_AGR_EMAIL("requisicao_contrato.etapa_agr.email"), REQUISICAO_CONTRATO_ETAPA_EDT_EMAIL(
			"requisicao_contrato.etapa_edt.email"), REQUISICAO_CONTRATO_ETAPA_VDJ_EMAIL(
					"requisicao_contrato.etapa_vdj.email"), REQUISICAO_CONTRATO_ETAPA_ADC_EMAIL(
							"requisicao_contrato.etapa_adc.email"), REQUISICAO_CONTRATO_ETAPA_RA1_EMAIL(
									"requisicao_contrato.etapa_ra1.email"), REQUISICAO_CONTRATO_ETAPA_RA2_EMAIL(
											"requisicao_contrato.etapa_ra2.email"), REQUISICAO_CONTRATO_ETAPA_RA3_EMAIL(
													"requisicao_contrato.etapa_ra3.email"), REQUISICAO_CONTRATO_ETAPA_RA4_EMAIL(
															"requisicao_contrato.etapa_ra4.email"), REQUISICAO_CONTRATO_STATUS_APR_EMAIL(
																	"requisicao_contrato.status_apr.email"),

	REQUISICAO_CONTRATO_ETAPA_AGR_SLA("requisicao_contrato.etapa_agr.sla"), REQUISICAO_CONTRATO_ETAPA_EDT_SLA(
			"requisicao_contrato.etapa_edt.sla"), REQUISICAO_CONTRATO_ETAPA_VDJ_SLA(
					"requisicao_contrato.etapa_vdj.sla"), REQUISICAO_CONTRATO_ETAPA_ADC_SLA(
							"requisicao_contrato.etapa_adc.sla"), REQUISICAO_CONTRATO_ETAPA_RA1_SLA(
									"requisicao_contrato.etapa_ra1.sla"), REQUISICAO_CONTRATO_ETAPA_RA2_SLA(
											"requisicao_contrato.etapa_ra2.sla"), REQUISICAO_CONTRATO_ETAPA_RA3_SLA(
													"requisicao_contrato.etapa_ra3.sla"), REQUISICAO_CONTRATO_ETAPA_RA4_SLA(
															"requisicao_contrato.etapa_ra4.sla"), REQUISICAO_CONTRATO_STATUS_APR_SLA(
																	"requisicao_contrato.status_apr.sla"),

	CONTRATO_VIGENCIA_EMAIL("contrato.vigencia.email"),

	CADASTRO_PARCEIRO_EMAIL("cadastro_parceiro.email"), CADASTRO_PARCEIRO_VENCIMENTO_AML_EMAIL(
			"cadastro_parceiro.vencimento_aml.email"), CADASTRO_PARCEIRO_VENCIDO_AML_EMAIL(
					"cadastro_parceiro.vencido_aml.email"), CADASTRO_PARCEIRO_ATIVACAO_EMAIL("cadastro_parceiro.ativacao.email"),

	NOTIFICACAO_EMAILS_BLOQUEADOS("notificacao.emails_bloqueados"),
	
	REQUISICAO_PAGINA("cadastro_venda.madeira")

	;

	private final String codigo;

	private ConfiguracaoEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
}