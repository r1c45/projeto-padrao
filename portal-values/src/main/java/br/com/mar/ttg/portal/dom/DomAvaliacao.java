package br.com.mar.ttg.portal.dom;

/**
 * 
 * @author gmazz
 *
 */
public class DomAvaliacao {

	public static class DomEtapaProcesso extends DomBase {
		public static final String GRAVACAO = "grv";
		public static final String PREPARACAO = "pre";
		public static final String REVISAO_METAS = "rev";
		public static final String FASE_1 = "fa1";
		public static final String FASE_2 = "fa2";
		public static final String FASE_3 = "fa3";
		public static final String FINALIZACAO = "fnl";

		DomEtapaProcesso() {
			mapa.put(GRAVACAO, "Gravação");
			mapa.put(PREPARACAO, "Preparação");
			mapa.put(REVISAO_METAS, "Revisão de metas");
			mapa.put(FASE_1, "Fase 1");
			mapa.put(FASE_2, "Fase 2");
			mapa.put(FASE_3, "Fase 3");
			mapa.put(FINALIZACAO, "Finalização");
		}
	}

	public static DomEtapaProcesso domEtapaProcesso = new DomEtapaProcesso();

	public static class DomTipo extends DomBase {
		public static final String AUTO = "A";
		public static final String INDEPENDENTE = "I";
		public static final String PARES = "P";
		public static final String EQUIPE = "E";
		public static final String GESTOR = "G";

		DomTipo() {
			mapa.put(AUTO, "Autoavaliação");
			mapa.put(INDEPENDENTE, "Independente");
			mapa.put(PARES, "Pares");
			mapa.put(EQUIPE, "Equipe");
			mapa.put(GESTOR, "Gestor");

		}
	}

	public static DomTipo domTipo = new DomTipo();

	public static class DomEtapa extends DomBase {
		public static final String PREENCHIMENTO_METAS = "pmt";
		public static final String REVISAO_METAS = "rmt";
		public static final String JUSTIFICACAO_METAS = "jmt";
		public static final String INDICACAO_AVALIADOR = "idv";
		public static final String RESPOSTA = "rpt";
		public static final String FINALIZACAO = "fnl";

		DomEtapa() {
			mapa.put(PREENCHIMENTO_METAS, "Preenchimento metas");
			mapa.put(REVISAO_METAS, "Revisão metas");
			mapa.put(JUSTIFICACAO_METAS, "Justificação metas");
			mapa.put(INDICACAO_AVALIADOR, "Indicação avaliador");
			mapa.put(RESPOSTA, "Resposta");
			mapa.put(FINALIZACAO, "Finalização");
		}
	}

	public static DomEtapa domEtapa = new DomEtapa();

	public static class DomAreaAtuacaoDesempenho extends DomBase {
		public static final String ADMINISTRATIVO = "adm";
		public static final String CERTIFICACAO = "crt";
		public static final String COMERCIAL = "cmr";
		public static final String FINANCEIRO = "fnc";
		public static final String GESTAO = "gst";
		public static final String GIS_TERRAS = "git";
		public static final String INVENTARIO = "ivt";
		public static final String JURIDICO = "jrd";
		public static final String OPERACOES_COLHEITA = "ocl";
		public static final String OPERACOES_SILVICULTURA = "osl";
		public static final String PLANEJAMENTO = "pnl";
		public static final String SOCIAL = "scl";

		DomAreaAtuacaoDesempenho() {
			mapa.put(ADMINISTRATIVO, "Administrativo");
			mapa.put(CERTIFICACAO, "Certificação");
			mapa.put(COMERCIAL, "Comercial");
			mapa.put(FINANCEIRO, "Financeiro");
			mapa.put(GESTAO, "Gestão");
			mapa.put(GIS_TERRAS, "Gis & Terras");
			mapa.put(INVENTARIO, "Inventário");
			mapa.put(JURIDICO, "Jurídico");
			mapa.put(OPERACOES_COLHEITA, "Operações Colheita");
			mapa.put(OPERACOES_SILVICULTURA, "Operações Silvicultura");
			mapa.put(PLANEJAMENTO, "Planejamento");
			mapa.put(SOCIAL, "Social");
		}
	}

	public static DomAreaAtuacaoDesempenho domAreaAtuacaoDesempenho = new DomAreaAtuacaoDesempenho();
}