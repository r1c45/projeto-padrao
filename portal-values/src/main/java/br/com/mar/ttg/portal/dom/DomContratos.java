package br.com.mar.ttg.portal.dom;

/**
 * 
 * @author ggraf
 *
 */
public class DomContratos {

	public static class DomStatusTipo extends DomBase {
		public static final String ATIVO = "ati";
		public static final String INATIVO = "ina";

		DomStatusTipo() {
			mapa.put(ATIVO, "Ativo");
			mapa.put(INATIVO, "Inativo");
		}
	}

	public static DomStatusTipo domStatusTipo = new DomStatusTipo();

	public static class DomTipoRequisicao extends DomBase {
		public static final String NOVO = "N";
		public static final String ADITIVO = "A";
		public static final String DISTRATO = "D";

		DomTipoRequisicao() {
			mapa.put(NOVO, "Novo");
			mapa.put(ADITIVO, "Aditivo");
			mapa.put(DISTRATO, "Distrato");
		}
	}

	public static DomTipoRequisicao domTipoRequisicao = new DomTipoRequisicao();

	public static class DomModeloRequisicao extends DomBase {
		public static final String PRESTACAO_SERVICO = "psv";
		public static final String VENDA = "ven";

		DomModeloRequisicao() {
			mapa.put(PRESTACAO_SERVICO, "Prestação Serviço");
			mapa.put(VENDA, "Venda");
		}
	}

	public static DomModeloRequisicao domModeloRequisicao = new DomModeloRequisicao();

	public static class DomStatusRequisicao extends DomBase {
		public static final String EM_ANDAMENTO = "ema";
		public static final String APROVADA = "apr";
		public static final String CANCELADA = "can";

		DomStatusRequisicao() {
			mapa.put(EM_ANDAMENTO, "Em Andamento");
			mapa.put(APROVADA, "Aprovada");
			mapa.put(CANCELADA, "Cancelada");
		}
	}

	public static DomStatusRequisicao domStatusRequisicao = new DomStatusRequisicao();

	public static class DomEtapaRequisicao extends DomBase {
		public static final String PREENCHIMENTO_REQUISICAO = "crq";
		public static final String APROVACAO_GERENTE = "agr";
		public static final String APROVACAO_PRESIDENCIA = "apd";
		public static final String ELABORACAO_DOCUMENTO = "edt";
		public static final String VALIDACAO_DOCUMENTO_JURIDICO = "vdj";
		public static final String APROVACAO_DOCUMENTO_CONSELHO = "adc";
		public static final String RECOLHIMENTO_ASSINATURA_DIRETORES = "ra1";
		public static final String RECEBIMENTO_ASSINADO_DIRETORES = "ra2";
		public static final String RECOLHIMENTO_ASSINATURA_PARCEIRO = "ra3";
		public static final String RECEBIMENTO_ASSINADO_TODAS_PARTES = "ra4";
		public static final String FINALIZACAO_PROCESSO = "fpc";
		public static final String SOLICITACAO_CANCELAMENTO = "scn";
		public static final String RECOLHIMENTO_ASSINATURA_ADM_REGIONAL = "raa";
		public static final String RECEBIMENTO_ASSINADO_ADM_REGIONAL = "rar";
		

		DomEtapaRequisicao() {
			mapa.put(PREENCHIMENTO_REQUISICAO, "Preenchimento da requisição");
			mapa.put(APROVACAO_GERENTE, "Envio da requisição para Aprovação do Gerente");
			mapa.put(APROVACAO_PRESIDENCIA, "Envio da requisição para Aprovação da Presidência");
			mapa.put(ELABORACAO_DOCUMENTO, "Elaboração do documento");
			mapa.put(VALIDACAO_DOCUMENTO_JURIDICO, "Envio do documento para chancela Jurídica");
			mapa.put(APROVACAO_DOCUMENTO_CONSELHO, "Envio do documento para aprovação do Conselho/Asset");
			mapa.put(RECOLHIMENTO_ASSINATURA_DIRETORES, "Envio do documento para assinatura do Fundo");
			mapa.put(RECEBIMENTO_ASSINADO_DIRETORES, "Retorno do documento assinado pelo Fundo");
			mapa.put(RECOLHIMENTO_ASSINATURA_PARCEIRO, "Envio do documento para assinatura do Parceiro");
			mapa.put(RECEBIMENTO_ASSINADO_TODAS_PARTES, "Retorno do documento assinado pelo Parceiro");
			mapa.put(FINALIZACAO_PROCESSO, "Documento finalizado, assinado pelas partes");
			mapa.put(SOLICITACAO_CANCELAMENTO, "Solicitação de cancelamento");
			mapa.put(RECOLHIMENTO_ASSINATURA_ADM_REGIONAL, "Envio do documento para assinatura da Adm. Regional");
			mapa.put(RECEBIMENTO_ASSINADO_ADM_REGIONAL, "Retorno do documento assinado pelo Adm. Regional");
		}
	}

	public static DomEtapaRequisicao domEtapaRequisicao = new DomEtapaRequisicao();

	public static class DomStatusEtapaRequisicao extends DomBase {
		public static final String PENDENTE = "pen";
		public static final String EM_ANDAMENTO = "ema";
		public static final String APROVADA = "apr";
		public static final String REPROVADA = "rep";
		public static final String PAUSADA = "psd";

		DomStatusEtapaRequisicao() {
			mapa.put(PENDENTE, "Pendente");
			mapa.put(EM_ANDAMENTO, "Em Andamento");
			mapa.put(APROVADA, "Aprovada");
			mapa.put(REPROVADA, "Reprovada");
			mapa.put(PAUSADA, "Pausada");
		}
	}

	public static DomStatusEtapaRequisicao domStatusEtapaRequisicao = new DomStatusEtapaRequisicao();

	public static class DomIndiceReajuste extends DomBase {
		public static final String IGPM = "igpm";
		public static final String IPCA = "ipca";
		public static final String INPC = "inpc";
		public static final String NA = "na";
		public static final String OUTROS = "outros";

		DomIndiceReajuste() {
			mapa.put(IGPM, "IGP-M");
			mapa.put(IPCA, "IPCA");
			mapa.put(INPC, "INPC");
			mapa.put(NA, "N/A");
			mapa.put(OUTROS, "Outros");
		}
	}

	public static DomIndiceReajuste domIndiceReajuste = new DomIndiceReajuste();

	public static class DomAreaAtuacao extends DomBase {
		public static final String ADMINISTRATIVO = "adm";
		public static final String AMBIENTAL = "amb";
		public static final String CERTIFICACAO = "crt";
		public static final String COMERCIAL = "cmr";
		public static final String FINANCEIRO = "fnc";
		public static final String GESTAO_CONTROLE = "gco";
		public static final String INVENTARIO = "ivt";
		public static final String JURIDICO = "jrd";
		public static final String OPERACOES_COLHEITA = "ocl";
		public static final String OPERACOES_SILVICULTURA = "osl";
		public static final String PLANEJAMENTO = "pnl";
		public static final String TERRAS_GIS = "tgs";

		DomAreaAtuacao() {
			mapa.put(ADMINISTRATIVO, "Administrativo");
			mapa.put(AMBIENTAL, "Ambiental");
			mapa.put(CERTIFICACAO, "Certificação");
			mapa.put(COMERCIAL, "Comercial");
			mapa.put(FINANCEIRO, "Financeiro");
			mapa.put(GESTAO_CONTROLE, "Gestão e Controle");
			mapa.put(INVENTARIO, "Inventário");
			mapa.put(JURIDICO, "Jurídico");
			mapa.put(OPERACOES_COLHEITA, "Operações Colheita");
			mapa.put(OPERACOES_SILVICULTURA, "Operações Silvicultura");
			mapa.put(PLANEJAMENTO, "Planejamento");
			mapa.put(TERRAS_GIS, "Terras/Gis");
		}
	}

	public static DomAreaAtuacao domAreaAtuacao = new DomAreaAtuacao();

	public static class DomTipoAssinatura extends DomBase {
		public static final String ADM_REGIONAL = "reg";
		public static final String PARCEIRO = "par";

		DomTipoAssinatura() {
			mapa.put(ADM_REGIONAL, "Administrador Regional");
			mapa.put(PARCEIRO, "Parceiro");
		}
	}

	public static DomTipoAssinatura domTipoAssinatura = new DomTipoAssinatura();
}