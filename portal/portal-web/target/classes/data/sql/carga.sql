-- CONFIGURACAO
INSERT INTO tb_configuracao VALUES ('usuario.senha_padrao', '1234');
INSERT INTO tb_configuracao VALUES ('email.usuario', 'gestao.portalttg@thetimbergroup.com');
INSERT INTO tb_configuracao VALUES ('email.senha', 'Wok67287');

INSERT INTO tb_configuracao VALUES ('notificacao.emails_bloqueados', 'carlos.guerreiro@thetimbergroup.com');

-- AVALIACAO
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.quantidade_itens_competencia', '14');
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.quantidade_itens_desempenho', '3');
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.destinatario_relatorio', 'ggraf@marinformatica.com.br');

INSERT INTO tb_configuracao VALUES ('processo_avaliacao.diretorio_arquivos', '/opt/ttg/avaliacao/arquivos');

INSERT INTO tb_configuracao VALUES ('processo_avaliacao.etapa_pre.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Ol&aacute;,<br /><br /> Conforme informado anteriormente, daremos in&iacute;cio ao processo #NOME_PROCESSO.</p>
<p>Nesta etapa de prepara&ccedil;&atilde;o, todos devem preencher as metas.<br /><br /> <span style="background-color: #ffff00;"><strong>Data limite para finaliza&ccedil;&atilde;o &ndash; Prepara&ccedil;&atilde;o &ndash; #DATA_PREVISTA</strong></span><br /><br /> Atentem-se ao prazo para que n&atilde;o ocorram falhas ou atrasos no processo e resultado final.</p>
<p><strong>Favor acessar a p&aacute;gina "Pessoas &gt; Avalia&ccedil;&atilde;o &gt; Processo"</strong> atrav&eacute;s do link abaixo.</p>
<p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p><p>Qualquer d&uacute;vida entre em contato atrav&eacute;s do e-mail <a href="mailto:suporte@marinformatica.com.br">suporte@marinformatica.com.br</a>.</p>');
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.etapa_fa1.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Ol&aacute;,&nbsp;</p>
<p>A prepara&ccedil;&atilde;o do processo #NOME_PROCESSO est&aacute; encerrada.</p>
<p>Nesta primeira fase, os gestores dever&atilde;o indicar algum colaborador de diferente &aacute;rea de atua&ccedil;&atilde;o (cliente interno) dentro da TTG, que far&aacute; a &ldquo;Avalia&ccedil;&atilde;o Independente&rdquo;. <br />N&atilde;o ser&aacute; permitida a indica&ccedil;&atilde;o em reciprocidade. Nesse caso, o sistema indicar&aacute; a necessidade de revis&atilde;o da escolha.</p>
<p><span style="background-color: #ffff00;"><strong>Data limite para finaliza&ccedil;&atilde;o &ndash; Fase 1 &ndash; #DATA_PREVISTA</strong></span></p>
<p><strong>Favor acessar a p&aacute;gina "Pessoas &gt; Avalia&ccedil;&atilde;o &gt; Indica&ccedil;&atilde;o"</strong> atrav&eacute;s do link abaixo.</p>
<p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p><p>Qualquer d&uacute;vida entre em contato atrav&eacute;s do e-mail <a href="mailto:suporte@marinformatica.com.br">suporte@marinformatica.com.br</a>.</p>');
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.etapa_fa2.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Ol&aacute;,&nbsp;</p>
<p>A Primeira Fase do processo #NOME_PROCESSO est&aacute; encerrada.</p>
<p>Daremos in&iacute;cio a Fase 2, com a realiza&ccedil;&atilde;o das&nbsp;avalia&ccedil;&otilde;es&nbsp;comportamentais (individual e gestor).</p>
<p>As demais orienta&ccedil;&otilde;es para preenchimento est&atilde;o dispon&iacute;veis no pr&oacute;prio formul&aacute;rio.</p>
<p><span style="background-color: #ffff00;"><strong>Data limite para finaliza&ccedil;&atilde;o &ndash; Fase 2 &ndash; #DATA_PREVISTA</strong></span></p>
<p><strong>Favor acessar a p&aacute;gina "Pessoas &gt; Avalia&ccedil;&atilde;o &gt; Processo"</strong> atrav&eacute;s do link abaixo.</p>
<p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p><p>Qualquer d&uacute;vida entre em contato atrav&eacute;s do e-mail <a href="mailto:suporte@marinformatica.com.br">suporte@marinformatica.com.br</a>.</p>');
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.etapa_fa3.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Ol&aacute;,&nbsp;</p>
<p>A Segunda Fase do processo #NOME_PROCESSO est&aacute; encerrada.</p>
<p>Daremos in&iacute;cio a Fase 3, com a realiza&ccedil;&atilde;o das&nbsp;avalia&ccedil;&otilde;es&nbsp;comportamentais (independente, par e equipe).</p>
<p>As demais orienta&ccedil;&otilde;es para preenchimento est&atilde;o dispon&iacute;veis no pr&oacute;prio formul&aacute;rio.</p>
<p><span style="background-color: #ffff00;"><strong>Data limite para finaliza&ccedil;&atilde;o &ndash; Fase 3 &ndash; #DATA_PREVISTA</strong></span></p>
<p><strong>Favor acessar a p&aacute;gina "Pessoas &gt; Avalia&ccedil;&atilde;o &gt; Processo"</strong> atrav&eacute;s do link abaixo.</p>
<p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p><p>Qualquer d&uacute;vida entre em contato atrav&eacute;s do e-mail <a href="mailto:suporte@marinformatica.com.br">suporte@marinformatica.com.br</a>.</p>');
INSERT INTO tb_configuracao VALUES ('processo_avaliacao.etapa_fnl.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Ol&aacute;,&nbsp;</p>
<p>Chegamos ao final do processo #NOME_PROCESSO.</p>
<p>Segue planilha contendo os resultados que tamb&eacute;m podem ser consultados pelo sistema atrav&eacute;s da p&aacute;gina <strong>"Pessoas &gt; Avalia&ccedil;&atilde;o &gt; Resultado"</strong> atrav&eacute;s do link abaixo.&nbsp;</p>
<p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');

-- REQUISICAO CONTRATO
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.diretorio_arquivos', '/opt/ttg/requisicao_contrato/arquivos');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.diretorio_historico', '/opt/ttg/requisicao_contrato/historico');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.diretorio_relatorio_mensal_gestor', '/opt/ttg/requisicao_contrato/relatorio/mensal_gestor');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.url_documentos', 'https://portalweb.ttgbrasil.com/pages/contratos/documentos.jsf?id=#ID');
INSERT INTO tb_configuracao VALUES ('tipo_contrato.diretorio_minutas', '/opt/ttg/tipo_contrato/minutas');

INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_agr.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
A requisi&ccedil;&atilde;o do documento de #TIPO_CONTRATO# entre #FUNDO_NOME# e&nbsp; #PARCEIRO_NOME# necessita de sua aprova&ccedil;&atilde;o.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_apd.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
A requisi&ccedil;&atilde;o do documento de #TIPO_CONTRATO# entre #FUNDO_NOME# e&nbsp; #PARCEIRO_NOME# necessita de sua aprova&ccedil;&atilde;o.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_edt.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# foi aprovado, e est&aacute; dispon&iacute;vel para elabora&ccedil;&atilde;o.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_vdj.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# foi elaborado, e est&aacute; dispon&iacute;vel para an&aacute;lise e chancela.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_adc.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# est&aacute; dispon&iacute;vel para aprova&ccedil;&atilde;o e assinatura.</p>#DETALHES#<p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra1.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# est&aacute; dispon&iacute;vel para impress&atilde;o, assinatura e reconhecimento de firma.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra2.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# foi assinado pela empresa solicitante e est&aacute; dispon&iacute;vel para ser enviado para a coleta 
da assinatura do parceiro de neg&oacute;cio.</p><p>Acesse o portal atrav&eacute;s do link abaixo.</p><p>
<a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra3.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# deve ser enviado para coleta de assinatura do parceiro de neg&oacute;cio.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra4.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# foi assinado e ser&aacute; enviado para o departamento de contratos.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_scn.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
Foi solicitado o cancelamento do documento de n&uacute;mero #NUMERO_CONTRATO#. Para realizar o cancelamento, favor acessar o portal.</p>#DETALHES#<p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.status_apr.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de n&uacute;mero #NUMERO_CONTRATO# entre foi assinado por todas as partes e est&aacute; dispon&iacute;vel no link abaixo.</p><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.status_can.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>
O documento de ID #ID# foi cancelado conforme solicita&ccedil;&atilde;o</p><p>Acesse o portal atrav&eacute;s do link abaixo.</p><p>
<a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.relatorio_mensal_gestor.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Prezado,</p>
<p>Segue anexo o Relat&oacute;rio do m&ecirc;s de #MES sobre a vig&ecirc;ncia dos contratos aprovados e em processo de formaliza&ccedil;&atilde;o.</p>
<p>Para a realiza&ccedil;&atilde;o de Distratos ou Aditivos, favor acessar o portal atrav&eacute;s do link abaixo.</p><p>
<a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');

INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_agr.sla', '2');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_apd.sla', '2');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_edt.sla', '3');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_vdj.sla', '3');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_adc.sla', '3');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra1.sla', '3');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra2.sla', '4');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra3.sla', '1');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_ra4.sla', '4');
INSERT INTO tb_configuracao VALUES ('requisicao_contrato.etapa_scn.sla', '2');

-- CONTRATO
INSERT INTO tb_configuracao VALUES ('contrato.vigencia.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>Prezados,</p>
<p>Faltam 40 dias para o t&eacute;rmino da vig&ecirc;ncia do contrato de n&uacute;mero #NUMERO_CONTRATO#.</p>#DETALHES#
<p>Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');

-- PARCEIRO
INSERT INTO tb_configuracao VALUES ('cadastro_parceiro.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}table, td{border: 2px solid #00457C; color: 
#00457C;}td{padding: 5px;}table{border-collapse: collapse;font-size:12px}.cabecalho{text-align: center; font-size: 20px; font-weight: 700; background-color: #00457C; 
color: #fff;}</style><p>Ol&aacute;.</p><p>Novo Parceiro de Neg&oacute;cio cadastrado no sistema. Consultar AML para parecer e/ou ativa&ccedil;&atilde;o.</p><table><tr>
<td colspan="2" class="cabecalho">Informa&ccedil;&otilde;es</td></tr><tr><td>Solicitante</td><td>#SOLICITANTE</td></tr><tr><td>Tipo</td><td>#TIPO_PARCEIRO</td></tr><tr><td>Nome</td><td>#NOME</td></tr><tr><td>CPF/CNPJ
</td><td>#CPF_CNPJ</td></tr><tr><td>Inscri&ccedil;&atilde;o Estadual</td><td>#INSCRICAO_ESTADUAL</td></tr><tr><td>E-mail</td><td>#EMAIL</td></tr><tr><td>Telefone</td>
<td>#TELEFONE</td></tr><tr><td>Tipo Atividade</td><td>#TIPO_ATIVIDADE</td></tr><tr><td colspan="2" class="cabecalho">Endere&ccedil;o</td></tr><tr><td>CEP</td>
<td>#ENDERECO_CEP</td></tr><tr><td>UF</td><td>#ENDERECO_UF</td></tr><tr><td>Cidade</td><td>#ENDERECO_CIDADE</td></tr><tr><td>Bairro</td><td>#ENDERECO_BAIRRO</td></tr>
<tr><td>Logradouro</td><td>#ENDERECO_LOGRADOURO</td></tr><tr><td>N&uacute;mero</td><td>#ENDERECO_NUMERO</td></tr><tr><td>Complemento</td><td>#ENDERECO_COMPLEMENTO</td>
</tr><tr><td colspan="2" class="cabecalho">S&oacute;cio 1</td></tr><tr><td>CPF/CNPJ</td><td>#SOCIO_1_CPF_CNPJ</td></tr><tr><td>Nome</td><td>#SOCIO_1_NOME</td></tr><tr>
<td>Data Nascimento</td><td>#SOCIO_1_DATA_NASCIMENTO</td></tr><tr><td colspan="2" class="cabecalho">S&oacute;cio 2</td></tr><tr><td>CPF/CNPJ</td><td>#SOCIO_2_CPF_CNPJ</td>
</tr><tr><td>Nome</td><td>#SOCIO_2_NOME</td></tr><tr><td>Data Nascimento</td><td>#SOCIO_2_DATA_NASCIMENTO</td></tr></table><p>
Acesse o portal atrav&eacute;s do link abaixo.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('cadastro_parceiro.vencimento_aml.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>A validação por AML do
 #TIPO_PARCEIRO #NOME vence em #DATA_VENCIMENTO.</p><p>Providenciar nova consulta.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('cadastro_parceiro.vencido_aml.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>A validação por AML do
 #TIPO_PARCEIRO #NOME venceu em #DATA_VENCIMENTO.</p><p>Providenciar nova consulta.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');
INSERT INTO tb_configuracao VALUES ('cadastro_parceiro.ativacao.email', '<meta charset="UTF-8"><style>body,p{font-family: Arial Narrow;}</style><p>O Parceiro de Negócios
 #NOME j&aacute; est&aacute; Ativo no Portal e dispon&iacute;vel para solicita&ccedil;&atilde;o de documentos.</p><p><a href="https://portalweb.ttgbrasil.com/">https://portalweb.ttgbrasil.com/</a></p>');