﻿INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_ACESSO_USUARIO_SISTEMA', 'Acesso ao cadastro de usuários sistema');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_ACESSO_USUARIO_COLABORADOR', 'Acesso ao cadastro de usuários colaboradores');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_ACESSO_USUARIO_EXTERNO', 'Acesso ao cadastro de usuários externos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_ACESSO_PERFIL', 'Acesso ao cadastro de perfis de usuário');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_AUXILIAR_DEPARTAMENTO', 'Acesso ao cadastro de departamentos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_AUXILIAR_NIVEL_HIERARQUIA', 'Acesso ao cadastro de níveis de hierarquia');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_AUXILIAR_BANCO', 'Acesso ao cadastro de bancos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_AUXILIAR_CODIGO_CONTABIL', 'Acesso ao cadastro de códigos contábeis');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_AVALIACAO_PROCESSO', 'Acesso ao cadastro de processo de avaliação');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_AVALIACAO_ESCALA', 'Acesso ao cadastro de escalas de avaliação');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_CLIENTE_FORNECEDOR', 'Acesso ao cadastro de parceiros cliente');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_FORNECEDOR', 'Acesso ao cadastro de parceiros fornecedor');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_FUNDO', 'Acesso ao cadastro de parceiros fundo');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_FUNDO_ASSET_MANAGER', 'Indicação de Asset Managers de fundos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_FUNDO_RESPONSAVEL_LEGAL', 'Indicação dos responsáveis legais de fundos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_ATIVACAO', 'Ativação do parceiro');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_INATIVACAO', 'Inativação do parceiro');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CADASTRO_PARCEIRO_ALTERACAO_TIPO', 'Alteração do tipo de parceiro');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_ADM_CONTRATOS_TIPO', 'Acesso ao cadastro de tipo de contrato');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_REQUISICAO', 'Acesso ao cadastro de requisições contrato');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_REQUISICAO_CONSULTA', 'Acesso a consulta de requisições contrato');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_CONTRATO_CONSULTA', 'Acesso a consulta de contratos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_HISTORICO', 'Acesso ao histórico de contratos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_HISTORICO_MANUTENCAO', 'Manutenção dos arquivos no histórico de contratos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_RELATORIO_ETAPAS', 'Relatório de etapas');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_CONTRATOS_SUBSTITUICAO_ARQUIVO_FINAL', 'Substituição do arquivo final do contrato');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_PESSOAS_AVALIACAO_CONSULTA', 'Acesso à consulta de avaliações');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_PESSOAS_AVALIACAO_INDICACAO', 'Acesso à indicação de avaliações');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_PESSOAS_AVALIACAO_RESULTADO', 'Acesso ao resultado de avaliações');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_PESSOAS_AVALIACAO_DASHBOARD', 'Acesso ao dashboard de avaliações');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_PESSOAS_CONTATOS', 'Acesso aos contatos');

INSERT INTO tb_permissao_acesso VALUES ('ROLE_DEPARTAMENTO_PRESIDENCIA', 'Departamento Presidência');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_DEPARTAMENTO_JURIDICO', 'Departamento Jurídico');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_DEPARTAMENTO_AUXILIAR_CONTRATOS', 'Auxiliar Departamento Contratos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_DEPARTAMENTO_CONTRATOS', 'Departamento Contratos');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_DEPARTAMENTO_FINANCEIRO', 'Departamento Financeiro');
INSERT INTO tb_permissao_acesso VALUES ('ROLE_DEPARTAMENTO_ADMINISTRATIVO', 'Departamento Administrativo');

INSERT INTO tb_perfil_usuario VALUES (NEXTVAL('sq_perfil_usuario'), 'Administrador');
INSERT INTO tb_perfil_usuario_permissao_acesso VALUES ((SELECT MAX(id) FROM tb_perfil_usuario), 'ROLE_ADM_ACESSO_PERFIL');

INSERT INTO tb_usuario VALUES (NEXTVAL('sq_usuario'),  'ggraf@marinformatica.com.br', 'Guilherme Graf', 'A6xnQhbz4Vx2HuGl4lXwZ5U2I8iziLRFnhP5eNfIRvQ=', (SELECT MAX(id) FROM tb_perfil_usuario), 'S', 'ati');
INSERT INTO tb_usuario_sistema VALUES ((SELECT MAX(id) FROM tb_usuario));