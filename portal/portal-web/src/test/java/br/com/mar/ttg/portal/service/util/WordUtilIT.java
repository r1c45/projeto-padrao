package br.com.mar.ttg.portal.service.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.hwpf.HWPFDocument;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author guilhermegraf
 *
 */
public class WordUtilIT {

	private Map<String, String> mapaValores;

	@Before
	public void setUp() {
		mapaValores = new HashMap<>();

		mapaValores.put("FUNDO_NOME", "Teste");
	}

	@Test
	public void testSubstituiValores() throws FileNotFoundException, IOException {
		File diretorio = new File("C:\\Users\\gmazz\\Desktop\\minutas");

		File[] arquivos = diretorio.listFiles();

		for (File arquivo : arquivos) {
			if (!arquivo.isFile() || arquivo.getName().contains("preenchido")) {
				continue;
			}

			HWPFDocument doc = new HWPFDocument(new FileInputStream(arquivo));
			WordUtil.substituiValores(doc, mapaValores);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			doc.write(out);

			byte[] conteudoElaborado = out.toByteArray();
			FileUtils.writeByteArrayToFile(
					new File(diretorio.getAbsolutePath() + File.separator + "preenchido_" + FilenameUtils.getBaseName(arquivo.getName()) + ".doc"),
					conteudoElaborado);
		}
	}
}