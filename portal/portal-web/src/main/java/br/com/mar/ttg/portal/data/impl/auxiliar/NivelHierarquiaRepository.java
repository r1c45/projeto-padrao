package br.com.mar.ttg.portal.data.impl.auxiliar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.auxiliar.NivelHierarquia;

/**
 * 
 * @author ggraf
 *
 */
@Repository
public class NivelHierarquiaRepository extends BaseRepository<NivelHierarquia> {

	public NivelHierarquiaRepository() {
		super(NivelHierarquia.class);
	}

	public List<NivelHierarquia> consulta(NivelHierarquia entity) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<NivelHierarquia> query = builder.createQuery(NivelHierarquia.class);
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<NivelHierarquia> table = query.from(NivelHierarquia.class);

		if (StringUtils.isNotEmpty(entity.getNome())) {
			predicateList.add(builder.and(builder.like(builder.upper(table.<String> get("nome")), "%" + entity.getNome().toUpperCase() + "%")));
		}

		query.orderBy(builder.asc(table.get("nome")));
		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<NivelHierarquia> typedQuery = getEntityManager().createQuery(query);

		return (List<NivelHierarquia>) typedQuery.getResultList();
	}
}