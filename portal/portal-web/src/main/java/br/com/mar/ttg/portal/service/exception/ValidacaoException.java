package br.com.mar.ttg.portal.service.exception;

/**
 * 
 * @author ggraf
 *
 */
public class ValidacaoException extends RuntimeException {

	private static final long serialVersionUID = -5918725246339147630L;

	public ValidacaoException(String message) {
		super(message);
	}
}