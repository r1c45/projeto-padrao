package br.com.mar.ttg.portal.web.util;

import java.util.ResourceBundle;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;

/**
 * 
 * @author ggraf
 *
 */
public class I18NUtil {

	public static String getMessage(String chave) {
		FacesContext context = FacesContext.getCurrentInstance();
		Application app = context.getApplication();
		ResourceBundle bundle = app.getResourceBundle(context, "messages");
		return bundle.getString(chave);
	}
}