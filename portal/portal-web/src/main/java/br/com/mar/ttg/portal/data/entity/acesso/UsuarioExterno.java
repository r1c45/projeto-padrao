package br.com.mar.ttg.portal.data.entity.acesso;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * 
 * @author gmazz
 *
 */
@Entity
@Table(name = "tb_usuario_externo")
@PrimaryKeyJoinColumn(name = "id")
public class UsuarioExterno extends Usuario {

	private static final long serialVersionUID = 2691107016842482377L;

	public UsuarioExterno() {
		setTipo(TIPO_EXTERNO);
	}
}