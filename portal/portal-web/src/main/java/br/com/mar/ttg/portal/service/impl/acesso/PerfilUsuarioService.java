package br.com.mar.ttg.portal.service.impl.acesso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.acesso.PerfilUsuario;
import br.com.mar.ttg.portal.data.impl.acesso.PerfilUsuarioRepository;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class PerfilUsuarioService extends CachedService {

	private static final String CACHE_NAME = "perfisUsuario";

	private static final String CACHE_KEY = "all";

	@Autowired
	private PerfilUsuarioRepository repository;

	@SuppressWarnings("unchecked")
	public List<PerfilUsuario> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

		if (null == cacheObj) {
			List<PerfilUsuario> objetos = repository.consulta(new PerfilUsuario());
			putOnCache(CACHE_NAME, CACHE_KEY, ObjectCopier.copy(objetos));

			cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

			return objetos;

		}

		return (List<PerfilUsuario>) ObjectCopier.copy(cacheObj);
	}

	public List<PerfilUsuario> consulta(PerfilUsuario entity) {
		return repository.consulta(entity);
	}

	public PerfilUsuario consultaPorId(Integer id) {
		return repository.findById(id);
	}
	
	public List<PerfilUsuario> consultaPerfil(){
		return repository.consultaPerfil();
	}

	public void cadastra(PerfilUsuario entity) {
		repository.store(entity);

		clearCache(CACHE_NAME);
	}

	public void atualiza(PerfilUsuario entity) {
		repository.update(entity);

		clearCache(CACHE_NAME);
	}
}