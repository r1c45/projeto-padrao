package br.com.mar.ttg.portal.web.controller.auxiliar;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.data.entity.auxiliar.Banco;
import br.com.mar.ttg.portal.service.exception.AlreadyExistsException;
import br.com.mar.ttg.portal.service.impl.auxiliar.BancoService;
import br.com.mar.ttg.portal.web.util.I18NUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
public class BancoController extends CrudBaseController<Banco, Banco> implements InitializingBean {

	private static final long serialVersionUID = 4666841801925003484L;

	@Autowired
	private BancoService service;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			setFilterEntity(new Banco());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	@Override
	protected boolean executeSave() {
		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_banco_atualizado"), null);

		} else {
			try {
				service.cadastra(getEntity());

				returnInfoMessage(I18NUtil.getMessage("sucesso_banco_cadastrado"), null);

			} catch (AlreadyExistsException e) {
				getEntity().setCodigo(null);
				returnWarnDialogMessage(I18NUtil.getMessage("aviso"), I18NUtil.getMessage("erro_banco_cadastrado"), null);
			}
		}

		return true;
	}

	@Override
	protected void beforeAdd() {
		setEntity(new Banco());
	}

	@Override
	protected void executeEdit(Banco entity) {
		setEntity(entity);
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consulta(getFilterEntity()));
	}
}