package br.com.mar.ttg.portal.web.controller.acesso;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.RequestScoped;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.data.entity.acesso.PerfilUsuario;
import br.com.mar.ttg.portal.data.entity.acesso.PermissaoAcesso;
import br.com.mar.ttg.portal.service.impl.acesso.PerfilUsuarioService;
import br.com.mar.ttg.portal.service.impl.acesso.PermissaoAcessoService;
import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.web.util.I18NUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@RequestScoped
public class PerfilUsuarioController extends CrudBaseController<PerfilUsuario, PerfilUsuario> implements InitializingBean {

	private static final long serialVersionUID = -5179328730959729197L;

	@Autowired
	private PerfilUsuarioService service;

	@Autowired
	private PermissaoAcessoService permissaoAcessoService;

	@Autowired
	private PermissaoAcessoService permissaoService;

	private DualListModel<PermissaoAcesso> permissoes;

	private List<PermissaoAcesso> permissoesDisponiveis;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			setFilterEntity(new PerfilUsuario());

			permissoesDisponiveis = permissaoAcessoService.consulta();

			permissoes = new DualListModel<PermissaoAcesso>(new ArrayList<PermissaoAcesso>(), new ArrayList<PermissaoAcesso>());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	@Override
	protected boolean executeSave() {
		getEntity().setPermissoes(permissoes.getTarget());

		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_perfil_atualizado"), null);

		} else {
			service.cadastra(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_perfil_cadastrado"), null);
		}

		return true;
	}

	@Override
	protected void beforeAdd() {
		setEntity(new PerfilUsuario());

		carregaPermissoes();
	}

	@Override
	protected void executeEdit(PerfilUsuario entity) {
		setEntity(entity);

		List<PermissaoAcesso> permissoesSelecionadas = permissaoService.consultaPorPerfilUsuario(getEntity().getId());

		carregaPermissoes();

		permissoes.getTarget().addAll(permissoesSelecionadas);

		for (PermissaoAcesso permissao : permissoes.getTarget()) {
			permissoes.getSource().remove(permissao);
		}
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consulta(getFilterEntity()));
	}

	// Util
	private void carregaPermissoes() {
		permissoes.getSource().clear();
		permissoes.getSource().addAll(permissoesDisponiveis);
		permissoes.getTarget().clear();
	}

	// Getters e Setters
	public DualListModel<PermissaoAcesso> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(DualListModel<PermissaoAcesso> permissoes) {
		this.permissoes = permissoes;
	}
}