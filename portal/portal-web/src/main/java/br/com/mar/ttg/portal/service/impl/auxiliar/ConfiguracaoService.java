package br.com.mar.ttg.portal.service.impl.auxiliar;

import java.io.Serializable;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.impl.auxiliar.ConfiguracaoRepository;
import br.com.mar.ttg.portal.enums.ConfiguracaoEnum;
import br.com.mar.ttg.portal.service.exception.NoResultFoundException;
import br.com.mar.ttg.portal.service.util.CachedService;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class ConfiguracaoService extends CachedService implements Serializable {

	private static final long serialVersionUID = -4054810010325009173L;

	private static final String CACHE_NAME = "configuracoes";

	@Autowired
	private ConfiguracaoRepository repository;

	public String consulta(ConfiguracaoEnum configuracao) {
		String codigo = configuracao.getCodigo();

		return consulta(codigo);
	}

	public String consulta(String codigo) {
		if (null == findOnCache(CACHE_NAME, codigo)) {
			try {
				String valor = repository.consulta(codigo);
				putOnCache(CACHE_NAME, codigo, valor);
				return valor;
			} catch (NoResultException e) {
				throw new NoResultFoundException();
			}
		} else {
			return (String) findOnCache(CACHE_NAME, codigo);
		}
	}
}