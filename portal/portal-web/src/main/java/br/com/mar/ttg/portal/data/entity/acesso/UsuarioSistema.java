package br.com.mar.ttg.portal.data.entity.acesso;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * 
 * @author gmazz
 *
 */
@Entity
@Table(name = "tb_usuario_sistema")
@PrimaryKeyJoinColumn(name = "id")
public class UsuarioSistema extends Usuario {

	private static final long serialVersionUID = -3213961337524551866L;

	public UsuarioSistema() {
		setTipo(TIPO_SISTEMA);
	}
}