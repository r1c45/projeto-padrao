package br.com.mar.ttg.portal.service.impl.auxiliar;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.auxiliar.NivelHierarquia;
import br.com.mar.ttg.portal.data.impl.auxiliar.NivelHierarquiaRepository;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class NivelHierarquiaService extends CachedService {

	private static final String CACHE_NAME = "niveisHierarquia";

	private static final String CACHE_KEY = "all";

	@Autowired
	private NivelHierarquiaRepository repository;

	@SuppressWarnings("unchecked")
	public List<NivelHierarquia> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

		if (null == cacheObj) {
			List<NivelHierarquia> objetos = repository.consulta(new NivelHierarquia());
			putOnCache(CACHE_NAME, CACHE_KEY, ObjectCopier.copy(objetos));

			cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

			return objetos;

		}

		return (List<NivelHierarquia>) ObjectCopier.copy(cacheObj);
	}

	public HashMap<Integer, NivelHierarquia> consultaMap() {
		List<NivelHierarquia> entities = consulta();

		HashMap<Integer, NivelHierarquia> entitiesMap = new HashMap<>();

		for (NivelHierarquia nivelHierarquia : entities) {
			entitiesMap.put(nivelHierarquia.getId(), nivelHierarquia);
		}

		return entitiesMap;
	}

	public List<NivelHierarquia> consulta(NivelHierarquia entity) {
		return repository.consulta(entity);
	}

	public NivelHierarquia consultaPorId(Integer id) {
		return repository.findById(id);
	}

	public void cadastra(NivelHierarquia entity) {
		repository.store(entity);

		clearCache(CACHE_NAME);
	}

	public void atualiza(NivelHierarquia entity) {
		repository.update(entity);

		clearCache(CACHE_NAME);
	}
}