package br.com.mar.ttg.portal.service.impl.acesso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.acesso.PermissaoAcesso;
import br.com.mar.ttg.portal.data.impl.acesso.PermissaoAcessoRepository;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class PermissaoAcessoService extends CachedService {

	private static final String CACHE_NAME = "permissoesAcesso";

	private static final String CACHE_KEY = "all";

	@Autowired
	private PermissaoAcessoRepository repository;

	@SuppressWarnings("unchecked")
	public List<PermissaoAcesso> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

		if (null == cacheObj) {
			List<PermissaoAcesso> objetos = repository.consulta();
			putOnCache(CACHE_NAME, CACHE_KEY, ObjectCopier.copy(objetos));

			cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

			return objetos;

		}

		return (List<PermissaoAcesso>) ObjectCopier.copy(cacheObj);
	}

	public List<PermissaoAcesso> consultaPorPerfilUsuario(Integer idPerfilUsuario) {
		return repository.consultaPorPerfilUsuario(idPerfilUsuario);
	}
}