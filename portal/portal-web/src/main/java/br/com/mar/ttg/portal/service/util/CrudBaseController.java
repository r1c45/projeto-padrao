package br.com.mar.ttg.portal.service.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Guilherme
 *
 * @param <S> Tipo do objeto para consulta
 * @param <E> Tipo do objeto para edicao
 *
 */
public abstract class CrudBaseController<S, E> extends SearchBaseController<S, E> {

	private static final long serialVersionUID = 7893886115215017656L;

	private boolean editing;
	private boolean editingRelated;

	private static final Logger LOGGER = LoggerFactory.getLogger(CrudBaseController.class);

	public void save() {
		try {
			if (!beforeSave()) {
				return;
			}
			if (!executeSave()) {
				return;
			}

			afterSave();

			editing = true;
		} catch (Throwable t) {
			if (editing) {
				returnFatalMessage("Erro ao alterar registro", null, t);
			} else {
				returnFatalMessage("Erro ao salvar registro", null, t);
			}
		}

		search();
	}

	protected boolean beforeSave() {
		LOGGER.debug("Antes salvar");
		return true;
	}

	protected abstract boolean executeSave();

	protected void afterSave() {
		LOGGER.debug("Depois salvar");
	}

	public void add() {
		setTabIndex(0);
		try {
			editing = false;
			beforeAdd();
		} catch (Throwable t) {
			returnFatalMessage("Erro ao adicionar novo registro", null, t);
		}
	}

	protected abstract void beforeAdd();

	public void edit(S entity) {
		try {
			editing = true;
			executeEdit(entity);
		} catch (Throwable t) {
			returnFatalMessage("Erro ao editar registro", null, t);
		}
	}

	protected abstract void executeEdit(S entity) throws Exception;

	public void editRelated(Object entity) {
		try {
			editingRelated = true;
			executeEditRelated(entity);
		} catch (Throwable t) {
			returnFatalMessage("Erro ao editar objeto relacionado", null, t);
		}
	}

	protected void executeEditRelated(Object relatedEntity) throws Exception {
	}

	@Override
	protected void select(Object entity) {
		// Nao necessario
	}

	@Override
	public void close() {
		editing = false;
		super.close();
	}

	// Getters and setters

	public void setSelectedEntity(S entity) {
		setTabIndex(0);
		edit(entity);
	}

	public void setSelectedRelatedEntity(Object relatedEntity) {
		editRelated(relatedEntity);
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public boolean isEditingRelated() {
		return editingRelated;
	}

	public void setEditingRelated(boolean editingRelated) {
		this.editingRelated = editingRelated;
	}
}
