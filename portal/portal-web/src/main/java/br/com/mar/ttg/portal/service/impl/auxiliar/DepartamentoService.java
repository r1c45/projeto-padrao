package br.com.mar.ttg.portal.service.impl.auxiliar;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mar.ttg.portal.data.entity.auxiliar.Departamento;
import br.com.mar.ttg.portal.data.impl.auxiliar.DepartamentoRepository;
import br.com.mar.ttg.portal.service.util.CachedService;
import br.com.mar.ttg.portal.service.util.ObjectCopier;
/**
 * 
 * @author ggraf
 *
 */
@Service
public class DepartamentoService extends CachedService {

	private static final String CACHE_NAME = "departamentos";

	private static final String CACHE_KEY = "all";

	@Autowired
	private DepartamentoRepository repository;

	@SuppressWarnings("unchecked")
	public List<Departamento> consulta() {
		Object cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

		if (null == cacheObj) {
			List<Departamento> objetos = repository.consulta(new Departamento());
			putOnCache(CACHE_NAME, CACHE_KEY, ObjectCopier.copy(objetos));

			cacheObj = findOnCache(CACHE_NAME, CACHE_KEY);

			return objetos;

		}

		return (List<Departamento>) ObjectCopier.copy(cacheObj);
	}

	public List<Departamento> consulta(Departamento entity) {
		return repository.consulta(entity);
	}

	public Departamento consultaPorId(Integer id) {
		return repository.findById(id);
	}

	public void cadastra(Departamento entity) {
		repository.store(entity);

		clearCache(CACHE_NAME);
	}

	public void atualiza(Departamento entity) {
		repository.update(entity);

		clearCache(CACHE_NAME);
	}
}