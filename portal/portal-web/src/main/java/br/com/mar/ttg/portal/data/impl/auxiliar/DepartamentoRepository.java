package br.com.mar.ttg.portal.data.impl.auxiliar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.auxiliar.Departamento;

/**
 * 
 * @author ggraf
 *
 */
@Repository
public class DepartamentoRepository extends BaseRepository<Departamento> {

	public DepartamentoRepository() {
		super(Departamento.class);
	}

	public List<Departamento> consulta(Departamento entity) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Departamento> query = builder.createQuery(Departamento.class);
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<Departamento> table = query.from(Departamento.class);

		if (StringUtils.isNotEmpty(entity.getNome())) {
			predicateList.add(builder.and(builder.like(builder.upper(table.<String> get("nome")), "%" + entity.getNome().toUpperCase() + "%")));
		}

		query.orderBy(builder.asc(table.get("nome")));
		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<Departamento> typedQuery = getEntityManager().createQuery(query);

		return (List<Departamento>) typedQuery.getResultList();
	}
}