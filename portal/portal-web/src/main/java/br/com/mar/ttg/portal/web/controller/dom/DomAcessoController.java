package br.com.mar.ttg.portal.web.controller.dom;

import java.io.Serializable;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.dom.DomAcesso;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@Scope("view")
public class DomAcessoController implements Serializable {

	private static final long serialVersionUID = -2121824805562763638L;

	public Map<String, Object> getStatusUsuarioMap() {
		return DomAcesso.domStatusUsuario.getMap();
	}
}