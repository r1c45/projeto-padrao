package br.com.mar.ttg.portal.data.entity.acesso;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author ggraf
 *
 */
@Entity
@Table(name = "tb_perfil_usuario")
public class PerfilUsuario implements Serializable {

	private static final long serialVersionUID = 6720331099939748519L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_PERFIL_USUARIO")
	@SequenceGenerator(name = "SQ_PERFIL_USUARIO", sequenceName = "SQ_PERFIL_USUARIO", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Integer id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "descricao")
	private String descricao;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "tb_perfil_usuario_permissao_acesso", joinColumns = {
			@JoinColumn(name = "id_perfil", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "cd_permissao", referencedColumnName = "codigo") })
	private List<PermissaoAcesso> permissoes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<PermissaoAcesso> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<PermissaoAcesso> permissoes) {
		this.permissoes = permissoes;
	}
}