package br.com.mar.ttg.portal.data.entity.auxiliar;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * @author ggraf
 *
 */
@Entity
@Table(name = "tb_nivel_hierarquia")
public class NivelHierarquia implements Serializable {

	private static final long serialVersionUID = 4390851399200902429L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_NIVEL_HIERARQUIA")
	@SequenceGenerator(name = "SQ_NIVEL_HIERARQUIA", sequenceName = "SQ_NIVEL_HIERARQUIA", allocationSize = 1)
	@Column(name = "id", nullable = false)
	private Integer id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "in_avaliacao_cruzada", nullable = false)
	private boolean avaliacaoCruzada;

	@Column(name = "in_auto_avaliacao", nullable = false)
	private boolean autoAvaliacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAvaliacaoCruzada() {
		return avaliacaoCruzada;
	}

	public void setAvaliacaoCruzada(boolean avaliacaoCruzada) {
		this.avaliacaoCruzada = avaliacaoCruzada;
	}

	public boolean isAutoAvaliacao() {
		return autoAvaliacao;
	}

	public void setAutoAvaliacao(boolean autoAvaliacao) {
		this.autoAvaliacao = autoAvaliacao;
	}
}