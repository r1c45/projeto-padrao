package br.com.mar.ttg.portal.service.util;


import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author ggraf
 *
 * @param <T>
 */
public class BaseRepository<T> {

	private Class<T> targetClass;

	public BaseRepository(Class<T> targetClass) {
		this.targetClass = targetClass;
	}

	protected EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	@PersistenceContext(unitName = "persistenceUnit")
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional(rollbackFor = Throwable.class)
	public void store(T entity) {
		getEntityManager().persist(entity);
	}

	@Transactional(rollbackFor = Throwable.class)
	public void update(T entity) {
		getEntityManager().merge(entity);
	}

	public T findById(Object id) {
		return getEntityManager().find(targetClass, id);
	}

	protected Integer generateId(String sequence) {
		Query query = getEntityManager().createQuery("select nextval('" + sequence + "')");
		return ((BigInteger) query.getSingleResult()).intValue();
	}

	@Transactional(rollbackFor = Throwable.class)
	public void delete(T entity) {
		getEntityManager().remove(getEntityManager().merge(entity));
	}

	protected String generateWhere(String where) {
		if (StringUtils.isEmpty(where)) {
			where += " WHERE ";
		} else {
			where += " AND ";
		}
		return where;
	}

	protected String generateWhere(String where, String clause) {
		String result = generateWhere(where);

		result += clause;

		return result;
	}
}
