package br.com.mar.ttg.portal.web.controller.dom;

import java.io.Serializable;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.dom.DomAvaliacao;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@Scope("view")
public class DomAvaliacaoController implements Serializable {

	private static final long serialVersionUID = 8119808138337418027L;

	public Map<String, Object> getEtapaProcessoMap() {
		return DomAvaliacao.domEtapaProcesso.getMap();
	}

	public Map<String, Object> getEtapaMap() {
		return DomAvaliacao.domEtapa.getMap();
	}

	public Map<String, Object> getTipoMap() {
		return DomAvaliacao.domTipo.getMap();
	}

	public Map<String, Object> getAreaAtuacaoDesempenhoMap() {
		return DomAvaliacao.domAreaAtuacaoDesempenho.getMap();
	}
}