package br.com.mar.ttg.portal.data.impl.acesso;

import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioLogin;

/**
 * @author ggraf
 *
 */
@Repository
public class UsuarioLoginRepository extends BaseRepository<UsuarioLogin> {

	public UsuarioLoginRepository() {
		super(UsuarioLogin.class);
	}

}