package br.com.mar.ttg.portal.web.controller.auxiliar;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.data.entity.auxiliar.NivelHierarquia;
import br.com.mar.ttg.portal.service.impl.auxiliar.NivelHierarquiaService;
import br.com.mar.ttg.portal.web.util.I18NUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
public class NivelHierarquiaController extends CrudBaseController<NivelHierarquia, NivelHierarquia> implements InitializingBean {

	private static final long serialVersionUID = -2744886475383699841L;

	@Autowired
	private NivelHierarquiaService service;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			setFilterEntity(new NivelHierarquia());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	@Override
	protected boolean executeSave() {
		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_nivel_atualizado"), null);

		} else {
			service.cadastra(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_nivel_cadastrado"), null);
		}

		return true;
	}

	@Override
	protected void beforeAdd() {
		setEntity(new NivelHierarquia());
	}

	@Override
	protected void executeEdit(NivelHierarquia entity) {
		setEntity(entity);
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consulta(getFilterEntity()));
	}
}