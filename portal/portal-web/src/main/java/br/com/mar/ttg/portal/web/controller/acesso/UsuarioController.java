	package br.com.mar.ttg.portal.web.controller.acesso;

import java.util.List;

import javax.faces.bean.RequestScoped;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.data.entity.acesso.PerfilUsuario;
import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioColaborador;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioExterno;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioSistema;
import br.com.mar.ttg.portal.data.entity.auxiliar.Departamento;
import br.com.mar.ttg.portal.data.entity.auxiliar.NivelHierarquia;
import br.com.mar.ttg.portal.dom.DomAcesso.DomEventoLogin;
import br.com.mar.ttg.portal.dom.DomAcesso.DomStatusUsuario;
import br.com.mar.ttg.portal.service.exception.AlreadyExistsException;
import br.com.mar.ttg.portal.service.exception.AvisoException;
import br.com.mar.ttg.portal.service.exception.ValidacaoException;
import br.com.mar.ttg.portal.service.impl.acesso.AuditoriaService;
import br.com.mar.ttg.portal.service.impl.acesso.PerfilUsuarioService;
import br.com.mar.ttg.portal.service.impl.acesso.UsuarioService;
import br.com.mar.ttg.portal.service.impl.auxiliar.DepartamentoService;
import br.com.mar.ttg.portal.service.impl.auxiliar.NivelHierarquiaService;
import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.web.util.I18NUtil;
import br.com.mar.ttg.portal.web.util.SessionUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@RequestScoped
public class UsuarioController extends CrudBaseController<Usuario, Usuario> implements InitializingBean {

	private static final long serialVersionUID = -66975495239952137L;

	private static final String VIEW_ALTERACAO_SENHA = "usuario/acesso/alteracao_senha.xhtml";

	private static final String VIEW_USUARIO_SISTEMA = "administracao/acesso/usuario/sistema.xhtml";

	private static final String VIEW_USUARIO_EXTERNO = "administracao/acesso/usuario/externo.xhtml";

	private static final String VIEW_CONTATOS = "pessoas/contatos.xhtml";

	private static final String VIEW_USUARIO_COLABORADOR = "administracao/acesso/usuario/colaborador.xhtml";

	@Autowired
	private UsuarioService service;

	@Autowired
	private PerfilUsuarioService perfilService;

	@Autowired
	private DepartamentoService departamentoService;

	@Autowired
	private NivelHierarquiaService nivelHierarquiaService;

	@Autowired
	private AuditoriaService auditoriaService;

	private List<PerfilUsuario> perfis;

	private List<Departamento> departamentos;

	private List<NivelHierarquia> niveisHierarquia;

	private List<Usuario> gestores;

	private String senhaAtual;

	private String novaSenha;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			
			perfis = perfilService.consulta();

			departamentos = departamentoService.consulta();

			niveisHierarquia = nivelHierarquiaService.consulta();

			setFilterEntity(new Usuario());


		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	@Override
	protected boolean executeSave() {
		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_usuario_atualizado"), null);

		} else {
			try {
				service.cadastra(getEntity());

				returnInfoMessage(I18NUtil.getMessage("sucesso_usuario_cadastrado"), null);

			} catch (AvisoException e) {
				returnWarnDialogMessage(I18NUtil.getMessage("aviso"), e.getMessage(), null);

			} catch (AlreadyExistsException e) {
				returnWarnDialogMessage(I18NUtil.getMessage("aviso"), I18NUtil.getMessage("erro_usuario_cadastrado"), null);
				return false;
			}
		}

		executeEdit(getEntity());

		return true;
	}

	@Override
	protected void beforeAdd() {
		if (isView(VIEW_USUARIO_COLABORADOR)) {
			setEntity(new UsuarioColaborador());

			gestores = service.consulta(new UsuarioColaborador());

		} else if (isView(VIEW_USUARIO_SISTEMA)) {
			setEntity(new UsuarioSistema());

		} else if (isView(VIEW_USUARIO_EXTERNO)) {
			setEntity(new UsuarioExterno());
		}
	}

	@Override
	protected void executeEdit(Usuario entity) {
		setEntity(entity);

		if (isView(VIEW_USUARIO_COLABORADOR)) {
			gestores = service.consulta(new UsuarioColaborador());

		}
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consultaUsuario(getFilterEntity()));
	}

	// Avaliacao
	public void cadastraAvaliacoes() {
		try {
			returnInfoDialogMessage(I18NUtil.getMessage("sucesso"), I18NUtil.getMessage("sucesso_cadastro_avaliacoes"));

			executeEdit(getEntity());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_cadastro_avaliacoes"), t);
		}
	}

	// Utilities
	public void reiniciaSenha() {
		try {
			service.reiniciaSenha(getEntity());

			returnInfoDialogMessage(I18NUtil.getMessage("sucesso"), I18NUtil.getMessage("sucesso_senha_reinicializacao"));

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_senha_reinicializacao"), t);

		}
	}

	public void alteraSenha() {
		try {
			//service.alteraSenha(getEntity().getId(), senhaAtual, novaSenha);

			returnInfoDialogMessage(I18NUtil.getMessage("sucesso"), I18NUtil.getMessage("sucesso_senha_alteracao"));

			auditoriaService.registraEventoLogin(SessionUtil.getIdLogin(), DomEventoLogin.TROCA_SENHA, null);

		} catch (ValidacaoException e) {
			returnWarnDialogMessage(I18NUtil.getMessage("aviso"), e.getMessage(), null);

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_senha_alteracao"), t);

		} finally {
			novaSenha = null;
			senhaAtual = null;
		}
	}

	// Relatorio


	// Getters e Setters
	public String getSenhaAtual() {
		return senhaAtual;
	}

	public List<PerfilUsuario> getPerfis() {
		return perfis;
	}

	public List<Departamento> getDepartamentos() {
		return departamentos;
	}

	public List<NivelHierarquia> getNiveisHierarquia() {
		return niveisHierarquia;
	}

	public List<Usuario> getGestores() {
		return gestores;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
}