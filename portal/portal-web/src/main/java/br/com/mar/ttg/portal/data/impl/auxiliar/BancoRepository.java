package br.com.mar.ttg.portal.data.impl.auxiliar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import br.com.mar.ttg.portal.service.util.BaseRepository;


import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.data.entity.auxiliar.Banco;

/**
 * 
 * @author ggraf
 *
 */
@Repository
public class BancoRepository extends BaseRepository<Banco> {

	public BancoRepository() {
		super(Banco.class);
	}

	public List<Banco> consulta(Banco entity) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Banco> query = builder.createQuery(Banco.class);
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<Banco> table = query.from(Banco.class);

		if (StringUtils.isNotEmpty(entity.getNome())) {
			predicateList.add(builder.and(builder.like(builder.upper(table.<String> get("nome")), "%" + entity.getNome().toUpperCase() + "%")));
		}

		query.orderBy(builder.asc(table.get("codigo")));
		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<Banco> typedQuery = getEntityManager().createQuery(query);

		return (List<Banco>) typedQuery.getResultList();
	}
}