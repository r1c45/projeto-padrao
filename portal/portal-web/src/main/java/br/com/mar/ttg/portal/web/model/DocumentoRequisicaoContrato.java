package br.com.mar.ttg.portal.web.model;

import java.io.Serializable;

/**
 * 
 * @author ggraf
 *
 */
public class DocumentoRequisicaoContrato implements Serializable {

	private static final long serialVersionUID = 4788267065780984388L;

	public DocumentoRequisicaoContrato(String codigoEtapa, String nomeArquivo, String nomeApresentacao) {
		super();
		this.codigoEtapa = codigoEtapa;
		this.nomeArquivo = nomeArquivo;
		this.nomeApresentacao = nomeApresentacao;
	}

	private String codigoEtapa;

	private String nomeArquivo;

	private String nomeApresentacao;

	public String getCodigoEtapa() {
		return codigoEtapa;
	}

	public void setCodigoEtapa(String codigoEtapa) {
		this.codigoEtapa = codigoEtapa;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public String getNomeApresentacao() {
		return nomeApresentacao;
	}

	public void setNomeApresentacao(String nomeApresentacao) {
		this.nomeApresentacao = nomeApresentacao;
	}
}