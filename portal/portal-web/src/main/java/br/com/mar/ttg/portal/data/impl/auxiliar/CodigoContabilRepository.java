package br.com.mar.ttg.portal.data.impl.auxiliar;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.auxiliar.CodigoContabil;

/**
 * 
 * @author ggraf
 *
 */
@Repository
public class CodigoContabilRepository extends BaseRepository<CodigoContabil> {

	public CodigoContabilRepository() {
		super(CodigoContabil.class);
	}

	public List<CodigoContabil> consulta(CodigoContabil entity) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<CodigoContabil> query = builder.createQuery(CodigoContabil.class);
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<CodigoContabil> table = query.from(CodigoContabil.class);

		if (StringUtils.isNotEmpty(entity.getDescricaoOperacao())) {
			predicateList.add(builder.and(
					builder.like(builder.upper(table.<String> get("descricaoOperacao")), "%" + entity.getDescricaoOperacao().toUpperCase() + "%")));
		}

		query.orderBy(builder.asc(table.get("codigo")));
		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<CodigoContabil> typedQuery = getEntityManager().createQuery(query);

		return (List<CodigoContabil>) typedQuery.getResultList();
	}
}