package br.com.mar.ttg.portal.web.controller.auxiliar;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.service.util.CrudBaseController;
import br.com.mar.ttg.portal.data.entity.auxiliar.Departamento;
import br.com.mar.ttg.portal.service.impl.auxiliar.DepartamentoService;
import br.com.mar.ttg.portal.web.util.I18NUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
public class DepartamentoController extends CrudBaseController<Departamento, Departamento> implements InitializingBean {

	private static final long serialVersionUID = 1888738671920944504L;

	@Autowired
	private DepartamentoService service;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			setFilterEntity(new Departamento());

		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_tela"), t);
		}
	}

	@Override
	protected boolean executeSave() {
		if (isEditing()) {
			service.atualiza(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_departamento_atualizado"), null);

		} else {
			service.cadastra(getEntity());

			returnInfoMessage(I18NUtil.getMessage("sucesso_departamento_cadastrado"), null);
		}

		return true;
	}

	@Override
	protected void beforeAdd() {
		setEntity(new Departamento());
	}

	@Override
	protected void executeEdit(Departamento entity) {
		setEntity(entity);
	}

	@Override
	protected void executeSearch() {
		setEntities(service.consulta(getFilterEntity()));
	}
}