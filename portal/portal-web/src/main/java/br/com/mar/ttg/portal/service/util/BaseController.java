package br.com.mar.ttg.portal.service.util;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 
 * @author ggraf
 *
 */
public class BaseController implements Serializable {

	private static final long serialVersionUID = -6314656601181178598L;

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

	//	Dialog Messages
	protected void returnInfoDialogMessage(String title, String message) {
		if (StringUtils.isEmpty(title)) {
			title = "AVISO";
		}

		returnDialogMessage(title, message, FacesMessage.SEVERITY_INFO);
	}

	protected void returnWarnDialogMessage(String title, String message, Throwable t) {
		if (StringUtils.isEmpty(title)) {
			title = "AVISO";
		}

		if (null != t) {
			LOGGER.warn(title, t);
		}

		returnDialogMessage(title, message, FacesMessage.SEVERITY_WARN);

	}

	protected void returnErrorDialogMessage(String title, String message, Throwable t) {
		if (StringUtils.isEmpty(title)) {
			title = "ERRO";
		}

		if (null != t) {
			LOGGER.error(title, t);
		}

		returnDialogMessage(title, message, FacesMessage.SEVERITY_ERROR);

	}

	protected void returnFatalDialogMessage(String title, String message, Throwable t) {
		if (StringUtils.isEmpty(title)) {
			title = "ERRO FATAL";
		}

		if (null != t) {
			LOGGER.error(title, t);
		}

		returnDialogMessage(title, message, FacesMessage.SEVERITY_FATAL);
	}

	private void returnDialogMessage(String title, String message, Severity severity) {
		RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(severity, title, message));
	}

	//	Messages
	protected void returnInfoMessage(String message, String target) {
		returnMessage(message, FacesMessage.SEVERITY_INFO, target);
	}

	protected void returnWarnMessage(String message, String target, Throwable t) {
		if (null != t) {
			LOGGER.warn("Aviso", t);
		}
		returnMessage(message, FacesMessage.SEVERITY_WARN, target);

	}

	protected void returnErrorMessage(String message, String target, Throwable t) {
		if (null != t) {
			LOGGER.error("Erro", t);
		}
		returnMessage(message, FacesMessage.SEVERITY_ERROR, target);

	}

	protected void returnFatalMessage(String message, String target, Throwable t) {
		if (null != t) {
			LOGGER.error("Erro fatal", t);
		}
		returnMessage(message, FacesMessage.SEVERITY_FATAL, target);
	}

	private void returnMessage(String message, Severity severity, String target) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (StringUtils.isNotEmpty(target)) {
			redirectView(target, context);
		}
		context.addMessage("message", new FacesMessage(severity, message, ""));
	}

	//	Dialogs
	protected void showDialog(String name) {
		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("PF('" + name + "').show()");
	}

	protected void hideDialog(String name) {
		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("PF('" + name + "').hide()");
	}

	//	Components
	protected void updateComponent(String id) {
		RequestContext rc = RequestContext.getCurrentInstance();
		rc.update(id);
	}

	protected void resetTable(String id) {
		DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
		if (null != dataTable) {
			dataTable.setSortBy(null);
			if (dataTable.isPaginator()) {
				dataTable.setPaginatorPosition("0");
			}
		}
	}

	// Utilities
	protected void redirectView(String target) {
		redirectView(target, null);
	}

	protected void redirectView(String target, FacesContext context) {
		if (null == context) {
			context = FacesContext.getCurrentInstance();
		}

		try {
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.getExternalContext().getFlash().setRedirect(true);
			context.getExternalContext().redirect(target);
		} catch (Throwable t) {
			LOGGER.error("Error redirecting page", t);
		}
	}

	protected String getApplicationUrl() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String url = request.getRequestURL().toString().replace(request.getRequestURI(), "") + request.getContextPath();

		return url;
	}

	public String getActualView() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (null == context || null == context.getViewRoot()) {
			return "";
		}
		return context.getViewRoot().getViewId();
	}



	public HttpSession getSession(boolean createSession) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(createSession);

		return session;
	}

	public Object getSessionAttribute(String name, boolean createSession) {
		return getSession(createSession).getAttribute(name);
	}

	public void setSessionAttribute(String name, Object value, boolean createSession) {
		getSession(createSession).setAttribute(name, value);
	}

	public String getClientIp() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return request.getRemoteAddr();
	}

	public String getSessionId() {
		return ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).getId();
	}

	public boolean isView(String name) {
		return getActualView().contains(name);
	}

	public UsernamePasswordAuthenticationToken getAuthenticatedUser() {
		UsernamePasswordAuthenticationToken user = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

		return user;
	}
}