package br.com.mar.ttg.portal.data.impl.acesso;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import br.com.mar.ttg.portal.service.util.BaseRepository;
import br.com.mar.ttg.portal.data.entity.acesso.Usuario;
import br.com.mar.ttg.portal.data.entity.acesso.UsuarioColaborador;

/**
 * 
 * @author ggraf
 *
 */
@Repository
public class UsuarioRepository extends BaseRepository<Usuario> {

	public UsuarioRepository() {
		super(Usuario.class);
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> consulta(Usuario entity) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<? extends Usuario> query = builder.createQuery(entity.getClass());
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<? extends Usuario> table = query.from(entity.getClass());

		if (StringUtils.isNotEmpty(entity.getNome())) {
			predicateList.add(builder.and(builder.like(builder.upper(table.<String> get("nome")), "%" + entity.getNome().toUpperCase() + "%")));
		}

		if (null != entity.getIdPerfil() && 0 != entity.getIdPerfil()) {
			predicateList.add(builder.and(builder.equal(table.get("idPerfil"), entity.getIdPerfil())));
		}

		if (StringUtils.isNotEmpty(entity.getStatus())) {
			predicateList.add(builder.and(builder.equal(table.get("status"), entity.getStatus())));
		}

		if (entity instanceof UsuarioColaborador) {
			if (null != ((UsuarioColaborador) entity).getIdNivelHierarquia() && 0 != ((UsuarioColaborador) entity).getIdNivelHierarquia()) {
				predicateList.add(builder.and(builder.equal(table.get("idNivelHierarquia"), ((UsuarioColaborador) entity).getIdNivelHierarquia())));
			}

			if (null != ((UsuarioColaborador) entity).getIdDepartamento() && 0 != ((UsuarioColaborador) entity).getIdDepartamento()) {
				predicateList.add(builder.and(builder.equal(table.get("idDepartamento"), ((UsuarioColaborador) entity).getIdDepartamento())));
			}

			if (null != ((UsuarioColaborador) entity).getIdGestor() && 0 != ((UsuarioColaborador) entity).getIdGestor()) {
				predicateList.add(builder.and(builder.equal(table.get("idGestor"), ((UsuarioColaborador) entity).getIdGestor())));
			}

		}

		query.orderBy(builder.asc(table.get("nome")));
		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<? extends Usuario> typedQuery = getEntityManager().createQuery(query);

		return (List<Usuario>) typedQuery.getResultList();
	}
	@SuppressWarnings("unchecked")
	public List<Usuario> consultaUsuario(Usuario entity) {
		String queryStr = "SELECT * FROM tb_usuario use";
		List<Object> params = new ArrayList<>();

		String where = "";
		

		if (null != entity.getNome()) {
			params.add(entity.getNome());
			where = generateWhere(where, "use.nome = ?");
		}

		if (null != entity.getIdPerfil()) {
			params.add(entity.getIdPerfil());
			where = generateWhere(where, "use.id_perfil = ?");
		}

		if (null != entity.getStatus()) {
			params.add(entity.getStatus());
			where = generateWhere(where, "use.status = ?");
		}
		
		queryStr += where;

		queryStr += " ORDER BY use.id";

		Query query = getEntityManager().createNativeQuery(queryStr);

		for (int i = 0; i < params.size(); i++) {
			query.setParameter((i + 1), params.get(i));
		}
		
		return (List<Usuario>) query.getResultList();

	}
	
	public Usuario consultaPorEmail(String email) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<? extends Usuario> query = builder.createQuery(Usuario.class);
		List<Predicate> predicateList = new ArrayList<Predicate>();

		Root<? extends Usuario> table = query.from(Usuario.class);

		predicateList.add(builder.and(builder.equal(table.get("email"), email)));

		query.where(predicateList.toArray(new Predicate[predicateList.size()]));
		TypedQuery<? extends Usuario> typedQuery = getEntityManager().createQuery(query);

		return typedQuery.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> consultaPorPermissao(String permissao) {
		Query query = getEntityManager().createNativeQuery(
				"SELECT usu.*, 0 AS clazz_ FROM tb_usuario usu INNER JOIN tb_perfil_usuario_permissao_acesso perm ON usu.id_perfil = perm.id_perfil"
						+ " WHERE perm.cd_permissao = ?",
				Usuario.class);

		query.setParameter(1, permissao);

		return query.getResultList();
	}

	public boolean possuiPermissao(Integer idUsuario, String permissao) {
		Query query = getEntityManager().createNativeQuery(
				"SELECT (COALESCE(COUNT(usu.id), 0) > 0) FROM tb_usuario usu INNER JOIN tb_perfil_usuario_permissao_acesso perm ON usu.id_perfil = perm.id_perfil"
						+ " WHERE usu.id = ? AND perm.cd_permissao = ?");

		query.setParameter(1, idUsuario);
		query.setParameter(2, permissao);

		return (boolean) query.getSingleResult();
	}
}