package br.com.mar.ttg.portal.service.exception;

/**
 * 
 * @author gmazz
 *
 */
public class AvisoException extends RuntimeException {

	private static final long serialVersionUID = 2258844356542129639L;

	public AvisoException(String message) {
		super(message);
	}
}