package br.com.mar.ttg.portal.web.controller;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import br.com.mar.ttg.portal.service.util.BaseController;
import br.com.mar.ttg.portal.web.util.I18NUtil;
import br.com.mar.ttg.portal.web.util.SessionUtil;

/**
 * 
 * @author ggraf
 *
 */
@Controller
@Scope("session")
public class SessionController extends BaseController implements InitializingBean {

	private static final long serialVersionUID = 8303028606794937818L;

	@Value("${version}")
	private String versaoSistema;

	@Value("${build.date}")
	private String dataBuild;

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
		} catch (Throwable t) {
			returnFatalDialogMessage(I18NUtil.getMessage("erro"), I18NUtil.getMessage("erro_sessao"), t);
		}
	}

	public boolean isAuthenticated() {
		if (null == SecurityContextHolder.getContext().getAuthentication()
				|| SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
			return false;
		}
		return true;
	}

	public static boolean possuiPermissao(String permissao) {
		return SessionUtil.possuiPermissao(permissao);
	}

	// Getters e Setters
	public String getVersaoSistema() {
		return versaoSistema;
	}

	public String getDataBuild() {
		return dataBuild;
	}
}