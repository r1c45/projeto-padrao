package br.com.mar.ttg.portal.service.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author ggraf
 *
 */
public class EncryptHelper {

	public static String encrypt(String originalValue, String algorithm) {
		if (StringUtils.isNotEmpty(originalValue)) {
			MessageDigest md;
			try {
				md = MessageDigest.getInstance(algorithm);
			} catch (NoSuchAlgorithmException e) {
				return "";
			}
			md.update(originalValue.getBytes());

			byte[] byteData = md.digest();
			String encoded = Base64.encodeBase64String(byteData);

			return encoded;
		} else {
			return "";
		}
	}
}