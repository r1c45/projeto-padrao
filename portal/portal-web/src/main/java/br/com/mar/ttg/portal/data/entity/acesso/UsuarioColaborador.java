package br.com.mar.ttg.portal.data.entity.acesso;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * 
 * @author gmazz
 *
 */
@Entity
@Table(name = "tb_usuario_colaborador")
@PrimaryKeyJoinColumn(name = "id")
public class UsuarioColaborador extends Usuario {

	private static final long serialVersionUID = 7705413932250523217L;

	public UsuarioColaborador() {
		setTipo(TIPO_COLABORADOR);
	}

	@Column(name = "id_nivel_hierarquia", nullable = false)
	private Integer idNivelHierarquia;

	@Column(name = "id_departamento")
	private Integer idDepartamento;

	@Column(name = "id_gestor")
	private Integer idGestor;

	@Column(name = "nm_cargo")
	private String nomeCargo;

	@Column(name = "codigo")
	private String codigo;

	public Integer getIdNivelHierarquia() {
		return idNivelHierarquia;
	}

	public void setIdNivelHierarquia(Integer idNivelHierarquia) {
		this.idNivelHierarquia = idNivelHierarquia;
	}

	public Integer getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Integer idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public Integer getIdGestor() {
		return idGestor;
	}

	public void setIdGestor(Integer idGestor) {
		this.idGestor = idGestor;
	}

	public String getNomeCargo() {
		return nomeCargo;
	}

	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}