package br.com.mar.ttg.portal.data.entity.acesso;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ggraf
 *
 */
@Entity
@Table(name = "tb_usuario_login")
public class UsuarioLogin implements Serializable {

	private static final long serialVersionUID = 2537721279040565540L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "id_usuario", nullable = false)
	private Integer idUsuario;

	@Column(name = "data", nullable = false)
	private Date data;

	@Column(name = "ip_origem", nullable = false)
	private String ipOrigem;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getIpOrigem() {
		return ipOrigem;
	}

	public void setIpOrigem(String ipOrigem) {
		this.ipOrigem = ipOrigem;
	}
}