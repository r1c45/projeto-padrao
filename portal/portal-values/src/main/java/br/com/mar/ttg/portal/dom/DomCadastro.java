package br.com.mar.ttg.portal.dom;

/**
 * 
 * @author ggraf
 *
 */
public class DomCadastro {

	public static class DomStatusParceiro extends DomBase {
		public static final String CADASTRADO = "cad";
		public static final String ATIVO = "ati";
		public static final String INATIVO = "ina";

		DomStatusParceiro() {
			mapa.put(CADASTRADO, "Cadastrado");
			mapa.put(ATIVO, "Ativo");
			mapa.put(INATIVO, "Inativo");
		}
	}

	public static DomStatusParceiro domStatusParceiro = new DomStatusParceiro();
	
	public static class DomTipoParceiro extends DomBase {
		public static final String CLIENTE = "C";
		public static final String FORNECEDOR = "F";
		public static final String CLIENTE_FORNECEDOR = "H";
		public static final String FUNDO = "U";

		DomTipoParceiro() {
			mapa.put(CLIENTE, "Cliente");
			mapa.put(FORNECEDOR, "Fornecedor");
			mapa.put(CLIENTE_FORNECEDOR, "Cliente/Fornecedor");
			mapa.put(FUNDO, "Fundo");
		}
	}

	public static DomTipoParceiro domTipoParceiro = new DomTipoParceiro();

	public static class DomEventoParceiro extends DomBase {
		public static final String CADASTRO = "cad";
		public static final String ATUALIZACAO = "atu";
		public static final String ATIVACAO = "ati";
		public static final String INATIVACAO = "ina";

		DomEventoParceiro() {
			mapa.put(CADASTRO, "Cadastro");
			mapa.put(ATUALIZACAO, "Atualização");
			mapa.put(ATIVACAO, "Ativação");
			mapa.put(INATIVACAO, "Inativação");
		}
	}

	public static DomEventoParceiro domEventoParceiro = new DomEventoParceiro();
}