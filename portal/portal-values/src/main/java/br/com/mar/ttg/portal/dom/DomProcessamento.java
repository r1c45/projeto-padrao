package br.com.mar.ttg.portal.dom;

/**
 * 
 * @author ggraf
 *
 */
public class DomProcessamento {

	public static class DomStatusNotificacao extends DomBase {
		public static final String PENDENTE = "P";
		public static final String ERRO = "R";
		public static final String ENVIADA = "E";
	}

	public static class DomTipoNotificacao extends DomBase {
		public static final String EMAIL = "E";
	}
}