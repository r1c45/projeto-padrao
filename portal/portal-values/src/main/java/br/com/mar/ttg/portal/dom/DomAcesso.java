package br.com.mar.ttg.portal.dom;

/**
 * 
 * @author ggraf
 *
 */
public class DomAcesso {

	public static class DomStatusUsuario extends DomBase {
		public static final String ATIVO = "ati";
		public static final String INATIVO = "ina";

		DomStatusUsuario() {
			mapa.put(ATIVO, "Ativo");
			mapa.put(INATIVO, "Inativo");
		}
	}

	public static DomStatusUsuario domStatusUsuario = new DomStatusUsuario();

	public static class DomEventoLogin extends DomBase {
		public static final String TROCA_SENHA = "trcsn";
	}

	public static class DomPermissao extends DomBase {
		public static final String DEPARTAMENTO_PRESIDENCIA = "ROLE_DEPARTAMENTO_PRESIDENCIA";
		public static final String DEPARTAMENTO_CONTRATOS = "ROLE_DEPARTAMENTO_CONTRATOS";
		public static final String DEPARTAMENTO_AUXILIAR_CONTRATOS = "ROLE_DEPARTAMENTO_AUXILIAR_CONTRATOS";
		public static final String DEPARTAMENTO_JURIDICO = "ROLE_DEPARTAMENTO_JURIDICO";
		public static final String DEPARTAMENTO_FINANCEIRO = "ROLE_DEPARTAMENTO_FINANCEIRO";
		public static final String DEPARTAMENTO_ADMINISTRATIVO = "ROLE_DEPARTAMENTO_ADMINISTRATIVO";
		public static final String PARCEIRO_RESPONSAVEL_LEGAL = "ROLE_ADM_CADASTRO_PARCEIRO_FUNDO_RESPONSAVEL_LEGAL";
		public static final String PARCEIRO_ASSET_MANAGER = "ROLE_ADM_CADASTRO_PARCEIRO_FUNDO_ASSET_MANAGER";
		public static final String PARCEIRO_ATIVACAO = "ROLE_ADM_CADASTRO_PARCEIRO_ATIVACAO";
		public static final String PARCEIRO_INATIVACAO = "ROLE_ADM_CADASTRO_PARCEIRO_INATIVACAO";
		public static final String PARCEIRO_ALTERACAO_TIPO = "ROLE_ADM_CADASTRO_PARCEIRO_ALTERACAO_TIPO";
		public static final String USUARIO_COLABORADOR = "ROLE_USUARIO_COLABORADOR";
		public static final String USUARIO_EXTERNO = "ROLE_USUARIO_EXTERNO";
		public static final String USUARIO_SISTEMA = "ROLE_USUARIO_SISTEMA";
		public static final String CONTRATOS_HISTORICO_MANUTENCAO = "ROLE_CONTRATOS_HISTORICO_MANUTENCAO";
		public static final String CONTRATOS_SUBSTITUICAO_ARQUIVO_FINAL = "ROLE_CONTRATOS_SUBSTITUICAO_ARQUIVO_FINAL";
	}
}