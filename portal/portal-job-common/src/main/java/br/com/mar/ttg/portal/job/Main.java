package br.com.mar.ttg.portal.job;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * @author ggraf
 *
 */
public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		new ClassPathXmlApplicationContext("job-context.xml");
	}
}