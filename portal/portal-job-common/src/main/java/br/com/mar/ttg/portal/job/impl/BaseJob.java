package br.com.mar.ttg.portal.job.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.sql.DataSource;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;

import br.com.mar.ttg.portal.dom.DomProcessamento.DomStatusNotificacao;
import br.com.mar.ttg.portal.enums.ConfiguracaoEnum;

/**
 * 
 * @author ggraf
 *
 */
public abstract class BaseJob extends QuartzJobBean {

	private static final String STATUS_ERRO = "E";
	private static final String STATUS_FINALIZADO = "F";

	protected Logger logger;

	protected Properties properties;

	private JdbcTemplate jdbcTemplate;

	private ClassPathXmlApplicationContext applicationContext;

	protected int quantidadeSucesso;

	protected int quantidadeErro;

	protected String caminhoArquivo;

	protected abstract void execute(ClassPathXmlApplicationContext applicationContext) throws IOException, SQLException;

	public void executeInternal(ClassPathXmlApplicationContext applicationContext) throws JobExecutionException {
		logger = LoggerFactory.getLogger(this.getClass());

		try {
			properties = PropertiesLoaderUtils.loadProperties(new InputStreamResource(new FileInputStream(("conf/job.properties"))));
		} catch (IOException e) {
			logger.error("Erro ao carregar configuracoes", e);
			return;
		}

		String codigoProcessamento = getCodigo();

		if (isRunning()) {
			logger.info("Ja existe um processamento " + codigoProcessamento + " em execucao");
			return;
		}

		Date dataInicio = new Date();
		try {
			setRunning(true);

			DataSource dataSource = (DataSource) applicationContext.getBean("dataSource");

			jdbcTemplate = new JdbcTemplate(dataSource);

			execute(applicationContext);

			audita(codigoProcessamento, dataInicio, STATUS_FINALIZADO);

		} catch (Throwable t) {
			logger.error("Erro durante o processamento " + codigoProcessamento, t);

			audita(codigoProcessamento, dataInicio, STATUS_ERRO);

		} finally {
			setRunning(false);
		}
	}

	private void audita(String codigoProcessamento, Date dataInicio, String status) {
		if (quantidadeSucesso > 0 || quantidadeErro > 0 || status.equals(STATUS_ERRO)) {
			jdbcTemplate.update(
					"INSERT INTO tb_processamento_auditoria(id, cd_processamento, dt_inicio, dt_fim, qt_sucesso, qt_erro, caminho_arquivo, status)"
							+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
					new Object[] { UUID.randomUUID().toString(), codigoProcessamento, dataInicio, new Date(), quantidadeSucesso, quantidadeErro,
							caminhoArquivo, status });
		}
	}

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		try {
			executeInternal((ClassPathXmlApplicationContext) context.getScheduler().getContext().get("applicationContext"));
		} catch (SchedulerException e) {
			throw new RuntimeException("Erro ao iniciar o contexto", e);
		}
	}

	protected abstract boolean isRunning();

	protected abstract void setRunning(boolean running);

	protected abstract String getCodigo();

	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public ClassPathXmlApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void enviaNotificacao(String tipo, String titulo, String conteudo, String[] destinatarios, String[] destinatariosCopia, File[] anexos) {
		String destinatariosStr = "";
		String destinatariosCopiaStr = null;
		String anexosStr = "";

		for (String destinatario : destinatarios) {
			destinatariosStr += destinatario + ";";
		}

		if (null != destinatariosCopia && destinatariosCopia.length > 0) {
			destinatariosCopiaStr = "";
			for (String destinatario : destinatariosCopia) {
				destinatariosCopiaStr += destinatario + ";";
			}
		}

		if (null != anexos) {
			for (File anexo : anexos) {
				anexosStr += anexo.getAbsolutePath() + ";";
			}
		}

		String id = UUID.randomUUID().toString();

		jdbcTemplate.update(
				"INSERT INTO tb_processamento_notificacao (id, tipo, titulo, conteudo, destinatarios, destinatarios_copia, anexos, dt_solicitacao, status)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new Object[] { id, tipo, titulo, conteudo, destinatariosStr, destinatariosCopiaStr, anexosStr, new Date(),
						DomStatusNotificacao.PENDENTE });
	}

	public String consultaEmailGestor(Integer idColaborador) {
		Map<String, Object> row = jdbcTemplate.queryForMap("SELECT ges.email FROM tb_usuario ges"
				+ " INNER JOIN tb_usuario_colaborador usu_col ON ges.id = usu_col.id_gestor INNER JOIN tb_usuario col ON usu_col.id = col.id"
				+ " WHERE col.id = ?", new Object[] { idColaborador });

		return (String) row.get("email");
	}

	public String consultaEmailUsuario(Integer idUsuario) {
		Map<String, Object> row = jdbcTemplate.queryForMap("SELECT email FROM tb_usuario WHERE id = ?", new Object[] { idUsuario });

		return (String) row.get("email");
	}

	public List<String> consultaEmailsPorPermissao(String codigoPermissao) {
		List<Map<String, Object>> rows = jdbcTemplate
				.queryForList("SELECT email FROM tb_usuario usu INNER JOIN tb_perfil_usuario_permissao_acesso perm ON usu.id_perfil = perm.id_perfil"
						+ " WHERE perm.cd_permissao = ?", new Object[] { codigoPermissao });

		List<String> emails = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			emails.add((String) row.get("email"));
		}

		return emails;
	}

	protected String consultaConfiguracao(ConfiguracaoEnum configuracao) {
		return consultaConfiguracao(configuracao.getCodigo());
	}

	protected String consultaConfiguracao(String codigo) {
		return (String) getJdbcTemplate().queryForMap("SELECT valor FROM tb_configuracao WHERE codigo = ?", new Object[] { codigo }).get("valor");
	}
}